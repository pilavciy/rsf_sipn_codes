function load_PubMed()
    @JLD2.load string("./data/PubMed/graph-label.jld2") G gr_truth
    return G,gr_truth
end

function load_cora()
    @JLD2.load string("./data/cora/graph-label.jld2") G gr_truth
    return G,gr_truth
end

function load_citeseer()
    @JLD2.load string("./data/citeseer/graph-label.jld2") G gr_truth
    return G,gr_truth
end
