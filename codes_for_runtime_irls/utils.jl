function noisy_kband_signal_generator(L::SparseMatrixCSC{Float64,Int64},k::Int64,SNR::Float64,signorm::Float64)
    N = size(L,1)
    _, Uk, _ = KrylovKit.eigsolve(L, k, :SR, krylovdim = max(KrylovDefaults.krylovdim, 2*k))
    Uk = reduce(hcat, Uk[1:k])
    ## create a signal to filter
    x_fourier = randn(k)
    x = Uk*x_fourier
    x = signorm*x ./ norm(x)
    σ =  signorm / sqrt(N * SNR)
    x,x + randn(N)*σ
end
function generate_SBM(n, k, s, easiness)
    εc = (s-sqrt(s)) / (s + sqrt(s)*(k-1))
    ε = εc / easiness

    q2 = (ε*s*k) / (n-k+ε*n*(k-1))
    q1 = q2 / ε

    cint = ceil((n/k -1) * q1)
    cout = floor((n - n/k) * q2)

    G = stochastic_block_model(cint, cout, repeat([Int(n/k)], inner=k))
    gr_truth = repeat(1:k, inner=Int64(n/k))
    return G, gr_truth
end

function inverse_with_pol_approx(L, y::Array{Float64,1}, q::Float64, p::Int64; method="Krylov",lmax=0.0)
    if method == "Krylov"
        lmax, _, _ = KrylovKit.eigsolve(L, 1, :LM, issymmetric=true); lmax = lmax[1]
    elseif method == "dmax"
        lmax = convert(Float64, 2 * maximum(degree(g)))
    end
    # display(lmax)
    CH = gsp_compute_cheby_coeff(x -> q ./ (x .+ q), p, 0., lmax)
    gsp_cheby_op(L, CH, y, 0., lmax)# .- (- CH[1]/2 + sum(CH[1:2:end]) - sum(CH[2:2:end]) - 1.) * mean(y)
end



function smooth_rf_xbar(g :: SimpleGraph,q :: Float64,M::SparseMatrixCSC{Float64,Int64},Y::Vector{Float64};maxiter=Inf,abstol=0.0,reltol=0.0,verbose=false)

### SCALARS
    indr = 0.0
    n = nv(g)
### VECTORS
    xbar = zeros(size(Y));
    est = zeros(Float64,n)
    psize = zeros(Float64,n)
    next = zeros(Int64,n)
    root = zeros(Int64,n)
    res_vec = zeros(Float64,n)
### RANDOMIZATION OBJECTS
    rng = MersenneTwister();
    rand_array_size = 2*ne(g)
    p = zeros(rand_array_size)

    rand!(rng,p)
    counter = 1
    normy = norm(Y)
    d = degree(g)
    dq = d .+ q
    randneighbor::Int = 0
    rn::Float64 = 0.0

### SET TOLERANCE
    residual = norm(Y)
    tolerance = max(reltol * residual, abstol)

    while(indr < maxiter)
        root .= Int64(0)
        indr += 1.0

## Forest sampling and calculate xbar
        @inbounds for i in 1:n
            u = Int64(i)
            while (root[u] === 0)
                if(counter === rand_array_size)
                    rand!(rng,p)
                    counter = 1
                end
                rn = p[counter]*dq[u]
                randneighbor = unsafe_trunc(Int64,rn)
                randneighbor += 1
                # rn = p[counter]*(dq[u])
                # randneighbor = ceil(Int64,rn)
                counter += 1
                if (d[u] < randneighbor)
                    root[u] = u
                    next[u] = 0
                    est[u] = Y[u]
                    psize[u] = 1.0
                else
                    next[u] = g.fadjlist[u][randneighbor]
                    u = next[u]
                end
            end
            r = root[u]
            # Retrace steps, erasing loops
            u = Int64(i)
            while (root[u] === 0)
                est[r] += Y[u]
                psize[r] += 1.0
                root[u] = r
                u = next[u]
            end
        end
## Calculate the residual
        est ./= psize
        @inbounds @simd for i = 1 : n
            est[i] = est[root[i]]
        end
        axpy!(1.0,est,xbar)
        if(tolerance != 0.0)
            mul!(res_vec,M,xbar,1.0/indr,0)
            mul!(res_vec,I,Y,1.0,-1.0)
            if(norm(res_vec) <= tolerance)
                break
            end
        end
    end
    xbar /= indr
    if(verbose)
        display("Iterations taken:$indr, Residual error: $(norm(res_vec))")
    end
    (est=xbar,nrep=Int(indr))
end

function smooth_rf_xbar(g :: SimpleGraph,q :: Array{Float64},M::SparseMatrixCSC{Float64,Int64},Y::Vector{Float64};maxiter=Inf,abstol=0.0,reltol=0.0,verbose=false)

### SCALARS
    indr = 0.0
    n = nv(g)
### VECTORS
    xbar = zeros(size(Y));
    est = zeros(Float64,n)
    psize = zeros(Float64,n)
    next = zeros(Int64,n)
    root = zeros(Int64,n)
    res_vec = zeros(Float64,n)
### RANDOMIZATION OBJECTS
    rng = MersenneTwister();
    rand_array_size = 2*ne(g)
    p = zeros(rand_array_size)

    rand!(rng,p)
    counter = 1
    normy = norm(Y)
    d = degree(g)
    dq = d .+ q
    randneighbor::Int = 0
    rn::Float64 = 0.0

### SET TOLERANCE
    residual = norm(Y)
    tolerance = max(reltol * residual, abstol)

    while(indr < maxiter)
        root .= Int64(0)
        indr += 1.0

## Forest sampling and calculate xbar
        @inbounds for i in 1:n
            u = Int64(i)
            while (root[u] === 0)
                if(counter === rand_array_size)
                    rand!(rng,p)
                    counter = 1
                end
                rn = p[counter]*dq[u]
                randneighbor = unsafe_trunc(Int64,rn)
                randneighbor += 1
                # rn = p[counter]*(dq[u])
                # randneighbor = ceil(Int64,rn)
                counter += 1
                if (d[u] < randneighbor)
                    root[u] = u
                    next[u] = 0
                    est[u] =  q[u]*Y[u]
                    psize[u] = q[u]
                else
                    next[u] = g.fadjlist[u][randneighbor]
                    u = next[u]
                end
            end
            r = root[u]
            # Retrace steps, erasing loops
            u = Int64(i)
            while (root[u] === 0)
                est[r] += q[u]*Y[u]
                psize[r] += q[u]
                root[u] = r
                u = next[u]
            end
        end
## Calculate the residual
        est ./= psize
        @inbounds @simd for i = 1 : n
            est[i] = est[root[i]]
        end
        axpy!(1.0,est,xbar)
        if(tolerance != 0.0)
            mul!(res_vec,M,xbar,1.0/indr,0)
            mul!(res_vec,I,Y,1.0,-1.0)
            if(norm(res_vec) <= tolerance)
                break
            end
        end
    end
    xbar /= indr
    if(verbose)
        display("Iterations taken:$indr, Residual error: $(norm(res_vec))")
    end
    (est=xbar,nrep=Int(indr))
end


function smooth_rf_xtilde(g :: SimpleGraph,q :: Float64,M::SparseMatrixCSC{Float64,Int64},Y::Vector{Float64};maxiter=Inf,abstol=0.0,reltol=0.0,verbose=false)

### SCALARS
    indr = 0.0
    n = nv(g)
### VECTORS
    xtilde = zeros(size(Y));
    est = zeros(Float64,n)
    next = zeros(Int64,n)
    root = zeros(Int64,n)
    res_vec = zeros(Float64,n)
### RANDOMIZATION OBJECTS
    rng = MersenneTwister();
    rand_array_size = 2*ne(g)
    p = zeros(rand_array_size)

    rand!(rng,p)
    counter = 1
    normy = norm(Y)
    d = degree(g)
    dq = d .+ q
    randneighbor::Int = 0
    rn::Float64 = 0.0

### SET TOLERANCE
    residual = norm(Y)
    tolerance = max(reltol * residual, abstol)

    while(indr < maxiter)
        root .= Int64(0)
        indr += 1.0

## Forest sampling and calculate xbar
        @inbounds for i in 1:n
            u = Int64(i)
            while (root[u] === 0)
                if(counter === rand_array_size)
                    rand!(rng,p)
                    counter = 1
                end
                rn = p[counter]*dq[u]
                randneighbor = unsafe_trunc(Int64,rn)
                randneighbor += 1
                # rn = p[counter]*(dq[u])
                # randneighbor = ceil(Int64,rn)
                counter += 1
                if (d[u] < randneighbor)
                    root[u] = u
                    next[u] = 0
                    est[u] = Y[u]
                else
                    next[u] = g.fadjlist[u][randneighbor]
                    u = next[u]
                end
            end
            r = root[u]
            # Retrace steps, erasing loops
            u = Int64(i)
            while (root[u] === 0)
                est[u] = Y[r]
                root[u] = r
                u = next[u]
            end
        end
## Calculate the residual

        axpy!(1.0,est,xtilde)
        if(tolerance != 0.0)
            mul!(res_vec,M,xtilde,1.0/indr,0)
            mul!(res_vec,I,Y,1.0,-1.0)
            if(norm(res_vec) <= tolerance)
                break
            end
        end
    end
    xtilde /= indr
    if(verbose)
        display("Iterations taken:$indr, Residual error: $(norm(res_vec))")
    end
    (est=xtilde,nrep=Int(indr))
end
function smooth_rf_xtilde(g :: SimpleGraph,q :: Array{Float64},M::SparseMatrixCSC{Float64,Int64},Y::Vector{Float64};maxiter=Inf,abstol=0.0,reltol=0.0,verbose=false)

### SCALARS
    indr = 0.0
    n = nv(g)
### VECTORS
    xtilde = zeros(size(Y));
    est = zeros(Float64,n)
    next = zeros(Int64,n)
    root = zeros(Int64,n)
    res_vec = zeros(Float64,n)
### RANDOMIZATION OBJECTS
    rng = MersenneTwister();
    rand_array_size = 2*ne(g)
    p = zeros(rand_array_size)

    rand!(rng,p)
    counter = 1
    normy = norm(Y)
    d = degree(g)
    dq = d .+ q
    randneighbor::Int = 0
    rn::Float64 = 0.0

### SET TOLERANCE
    residual = norm(Y)
    tolerance = max(reltol * residual, abstol)

    while(indr < maxiter)
        root .= Int64(0)
        indr += 1.0

## Forest sampling and calculate xbar
        @inbounds for i in 1:n
            u = Int64(i)
            while (root[u] === 0)
                if(counter === rand_array_size)
                    rand!(rng,p)
                    counter = 1
                end
                rn = p[counter]*dq[u]
                randneighbor = unsafe_trunc(Int64,rn)
                randneighbor += 1
                # rn = p[counter]*(dq[u])
                # randneighbor = ceil(Int64,rn)
                counter += 1
                if (d[u] < randneighbor)
                    root[u] = u
                    next[u] = 0
                    est[u] = Y[u]
                else
                    next[u] = g.fadjlist[u][randneighbor]
                    u = next[u]
                end
            end
            r = root[u]
            # Retrace steps, erasing loops
            u = Int64(i)
            while (root[u] === 0)
                est[u] = Y[r]
                root[u] = r
                u = next[u]
            end
        end
## Calculate the residual

        axpy!(1.0,est,xtilde)
        if(tolerance != 0.0)
            mul!(res_vec,M,xtilde,1.0/indr,0)
            mul!(res_vec,I,Y,1.0,-1.0)
            if(norm(res_vec) <= tolerance)
                break
            end
        end
    end
    xtilde /= indr
    if(verbose)
        display("Iterations taken:$indr, Residual error: $(norm(res_vec))")
    end
    (est=xtilde,nrep=Int(indr))
end
