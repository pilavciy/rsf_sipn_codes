function gsp_compute_cheby_coeff(f, p::Int64, λmin::Float64, λmax::Float64)
    r"""
    Compute Chebyshev coefficients for a filter f.

    Parameters
    ----------
    f : filter
    p : Int64
        Maximum order of Chebyshev coeff to compute
        (default = 30)
    λmax : largest eigenvalue of the Laplacian

    Returns
    -------
    c : ndarray
        Matrix of Chebyshev coefficients
    """
    N = p + 1

    a1 = (λmax - λmin) / 2
    a2 = (λmax + λmin) / 2

    c = zeros(N)

    tmpN = collect(1:N) .- 0.5
    aux = f.(a1 * cos.(pi * tmpN / N) .+ a2)
    for i = 0:p
        c[i+1] = 2 * aux' * cos.(pi * i * tmpN / N) / N
    end
    return c
end

function gsp_cheby_eval(x::Array{Float64,1}, c::Array{Float64,1}, λmin::Float64, λmax::Float64)
    r = gsp_cheby_op(spdiagm(0 => x), c, ones(size(x)), λmin, λmax)
    return r[:]
end

function gsp_cheby_op(L, c::Array{Float64,1}, signal::Array{Float64,1}, λmin::Float64, λmax::Float64)
    r"""
    Chebyshev polynomial of graph Laplacian applied to vector.

    Parameters
    ----------
    L: Laplacian
    c : ndarray or list of ndarrays
        Chebyshev coefficients for a Filter
    signal : ndarray
        Signal to filter
    λmax : max eigenvalue of Laplacian

    Returns
    -------
    r : ndarray
        Result of the filtering

    """
    N = size(L,1)
    M = size(c,1)

    arange = [λmin, λmax]

    a1 = (arange[2] - arange[1]) / 2
    a2 = (arange[2] + arange[1]) / 2

    twf_old = signal
    twf_cur = (L * signal .- a2 * signal) ./ a1

    r = 0.5 .* c[1] .* twf_old .+ c[2] .* twf_cur

    for k = 2:M-1
        twf_new = (2/a1) * (L * twf_cur - a2 * twf_cur) - twf_old
        r += c[k+1] .* twf_new
        twf_old = twf_cur
        twf_cur = twf_new
    end
    return r
end
