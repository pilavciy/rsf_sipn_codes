using RandomForests
using ImageView, TestImages, Gtk.ShortNames, Images, ImageMagick, ImageQualityIndexes
using DelimitedFiles
using LightGraphs,SimpleWeightedGraphs
using StatsBase
using Distributions
using SparseArrays
using LinearAlgebra
using JLD2
using Noise
using PyPlot
using Convex,SCS,ECOS

pygui(true)

include("../../utils.jl")

outputfolder = "./output/imagedenoising/"
println("Output Folder: $outputfolder")
try
    mkdir(outputfolder)
catch
    println("output folder already exists")
end
    #### GAUSSIAN NOISE and L1 REGULARIZATION #####
    imname = "house"
    image = imresize(testimage(imname), 128, 128)
    image = Float64.(Gray.(image))
    nx = size(image,1)
    ny = size(image,2)
    rs = (v) -> reshape(v,nx,ny)
    G = LightGraphs.grid([nx,ny])
    B = Matrix(incidence_matrix(G,oriented=true)')
    L = (laplacian_matrix(G)) + 10^(-14)*I
    image = image[:]
    nrep = 20

    σ = 0.2
    im_noisy = image .+ randn(length(image))*σ
    mu_range = 0.1:0.1:10.0
    psnr_range = zeros(length(mu_range))
    Xconvex = zeros(length(mu_range),nx*ny)
    for (i,mu) in enumerate(mu_range)
        xconvex = Variable(nx*ny)
        display(mu)
        problem = minimize(mu*sumsquares(xconvex-im_noisy) + norm(B*xconvex,1) )
        solve!(problem, () -> ECOS.Optimizer(verbose=true,max_it=1000))
        xconvex = Convex.evaluate(xconvex)
        Xconvex[i,:] = xconvex
        xconvex = rs(xconvex)
        psnr_range[i] = (ImageQualityIndexes.assess_psnr(xconvex, image))
    end
    mu = mu_range[argmax(psnr_range)]
    xconvex = rs(Xconvex[argmax(psnr_range),:])


    xhat,loss_hat = irls(G,im_noisy,rand(nx*ny),mu;numofiter = 20,tol=0.0, method="exact",status=true)
    xhat = rs(xhat)

    xtilde,loss_tilde =irls(G,im_noisy,im_noisy,mu;numofiter = 20,tol=0.0,nrep=nrep, method="xtilde",status=true)
    xtilde = rs(xtilde)

    xbar,loss_bar = irls(G,im_noisy,im_noisy,mu;numofiter = 20,tol=0.0,nrep=nrep, method="xbar",status=true)
    xbar = rs(xbar)


    y = rs(im_noisy)
    # y = y[:]
    noisy_psnr = (ImageQualityIndexes.assess_psnr(y, image))
    # xhat = xhat[:]
    xhat_psnr = (ImageQualityIndexes.assess_psnr(xhat, image))
    # xtilde = xtilde[:]
    xtilde_psnr = (ImageQualityIndexes.assess_psnr(xtilde, image))
    # xbar = xbar[:]
    xbar_psnr = (ImageQualityIndexes.assess_psnr(xbar, image))
    # xconvex = xconvex[:]
    xconvex_psnr = (ImageQualityIndexes.assess_psnr(xconvex, image))

    display("y psnr: $noisy_psnr")
    display("xhat psnr: $xhat_psnr")
    display("xtilde psnr : $xtilde_psnr")
    display("xbar psnr : $xbar_psnr")
    display("xconvex psnr : $xconvex_psnr")


    y[y.<0] .= 0.; y[y.>1] .= 1.
    xhat[xhat.<0] .= 0.; xhat[xhat.>1] .= 1.
    xtilde[xtilde.<0] .= 0.; xtilde[xtilde.>1] .= 1.
    xbar[xbar.<0] .= 0.; xbar[xbar.>1] .= 1.
    xconvex[xconvex.<0] .= 0.; xconvex[xconvex.>1] .= 1.

    save(string(outputfolder,"gaussian_orig.jpg"), rs(image))
    save(string(outputfolder,"gaussian_noisy.jpg"), (y))
    save(string(outputfolder,"gaussian_xhat_mu=$mu.jpg"), (xhat))
    save(string(outputfolder,"gaussian_xtilde_mu=$(mu)_nrep=$nrep.jpg"), (xtilde))
    save(string(outputfolder,"gaussian_xbar_mu=$(mu)_nrep=$nrep.jpg"), (xbar))
    save(string(outputfolder,"gaussian_xconvex_mu=$(mu).jpg"), (xconvex))


    fig = figure(figsize=[5,5])
    # plot((1:length(loss_tilde)), loss_tilde, color="blue",linewidth=6.0, label="Updates with x-tilde")
    plot((1:length(loss_bar)), loss_bar, color="orange",linewidth=6.0, label=string("Updates with ", latexstring("\$\\bar{x}\$")))
    plot((1:length(loss_hat)), loss_hat, color="black",linewidth=6.0, label=string("Updates with ", latexstring("\$\\hat{x}\$")))
    xlabel("Iterations",fontsize=20)
    ylabel("Loss Function",fontsize=20)
    xticks(fontsize=15)
    yticks(fontsize=15)
    PyPlot.grid(true)
    tight_layout()

    legend(fontsize=15)
    JLD2.@save(string("./output/imagedenoising/workspace.jld"),noisy_psnr,xhat_psnr,xtilde_psnr,xbar_psnr,xconvex_psnr)
