using LightGraphs,LinearAlgebra,Statistics, Distances
using KrylovKit,SparseArrays,Random
using PyPlot
using RandomForests
using BenchmarkTools
using Profile
using LinearAlgebra
using NearestNeighbors
using AlgebraicMultigrid
using IterativeSolvers
using JLD2
using PyCall
pygui(true)


include("../../utils.jl")
include("../../ssl_data_loaders.jl")
include("../../cheb_pol_utils.jl")
LinearAlgebra.BLAS.set_num_threads(1)


## CODE STARTS HERE
N = 10000 # number of nodes

nsamples = 10
neval = 10
BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
BenchmarkTools.DEFAULT_PARAMETERS.evals = neval
BenchmarkTools.DEFAULT_PARAMETERS.samples = nsamples

graphs = ["Grid","Barabasi-Albert","Erdos-Renyi","K-regular","Euclidean","Bunny","Cora","Citeseer"]

# graphs = ["Grid","Barabasi-Albert","Erdos-Renyi","K-regular","SBM","Euclidean","Bunny","Cora","Citeseer","Pubmed"]
# graphs = ["Pubmed","SBM"]
for gname in graphs
    ## choose graph
    display(gname)
    if(gname == "Path")
        g = LightGraphs.path_graph(N);
    elseif(gname == "Star")
        g = LightGraphs.star_graph(N);
    elseif(gname == "Grid")
        g = LightGraphs.grid([100,Int(N/100)], periodic=true);
    elseif(gname == "Barabasi-Albert")
        g = barabasi_albert(N, 2)
    elseif(gname == "Erdos-Renyi")
        avgd = 10
        p = avgd/N
        g = erdos_renyi(N, p)
    elseif(gname == "K-regular")
        d = 10
        g = random_regular_graph(N,d)
    elseif(gname == "SBM")
        num_blocks = 5 #number of classes
        av_degree = 30.
        easiness = 10.
        g, _ = generate_SBM(N, num_blocks, av_degree, easiness)
    elseif(gname == "Euclidean")
        d = 3
        k = 20
        pts = rand(d, N); # N vertices in [0,1]^d
        kdtree = KDTree(pts)
        g = SimpleGraph(N)
        for i=1:N
            idxs, _ = knn(kdtree, pts[:,i], k+1)
            for j in setdiff!(idxs,i)
                add_edge!(g, i, j)
            end
        end
    elseif(gname == "Bunny")
        py"""
        import numpy as np
        from pygsp import graphs
        A = graphs.Bunny().A.todense()
        """
        A = PyArray(py"A"o)
        g = SimpleGraph(A)

    elseif(gname == "Cora")
        g , _ = load_cora()
    elseif(gname == "Citeseer")
        g , _ = load_citeseer()
    elseif(gname == "Pubmed")
        g , _ = load_PubMed()
    end
    N = nv(g)
    display("Num of nodes : $(nv(g)). Num of edges : $(ne(g)).")
    @assert is_connected(g)
    ## create Laplacian matrix:

    L = laplacian_matrix(g) + 10^(-14)*I
    l = eigvals(Matrix(L))

    ## Signal generation parameters
    sigRep = 20
    SNR = 2.0
    k = 5
    signorm = 1.0

    ## Initializations for recording results
    Y = zeros(N,sigRep)
    X = zeros(N,sigRep)
    Xhat = zeros(N,sigRep)

    avg_rec_err_xhat = 0
    avg_q = 0

    ### Experiment params
    NiterRange = unique(round.(Int64,10 .^ range(0,stop=2,length=20)))
    comp_time_pol = zeros(length(NiterRange))
    app_err_pol = zeros(length(NiterRange))
    rec_err_pol = zeros(length(NiterRange))

    comp_time_cg = zeros(length(NiterRange))
    app_err_cg = zeros(length(NiterRange))
    rec_err_cg = zeros(length(NiterRange))

    comp_time_pcg = zeros(length(NiterRange))
    app_err_pcg = zeros(length(NiterRange))
    rec_err_pcg = zeros(length(NiterRange))

    comp_time_rf = zeros(length(NiterRange))
    app_err_rf = zeros(length(NiterRange))
    rec_err_rf = zeros(length(NiterRange))

    comp_time_rf_tilde = zeros(length(NiterRange))
    app_err_rf_tilde = zeros(length(NiterRange))
    rec_err_rf_tilde = zeros(length(NiterRange))

    q_arr = zeros((sigRep))
    comp_time_backslash = 0

    global x_backslash = zeros(N)

    for sigiter = 1 : sigRep

    ## create a signal to filter
        x,y = noisy_kband_signal_generator(L,k,SNR,signorm)
        Y[:,sigiter] = y
        X[:,sigiter] = x
        ## Calculate the mean for mean correction and subtract it

        display("$gname-$sigiter Initial SNR:$(((norm(x) / norm(y-x)))^2)")
        display("$gname-$sigiter Signal norm:$(norm(x))")
        if(sigiter == 1 )
            figure()
            plot(y)
            plot(x)
        end
        ## Parameter Selection
        q_range = round.(10 .^ (-4:1.0:5),sigdigits=2)
        rec_err_xhat = zeros(length(q_range))
        for (i,q) in enumerate(q_range)
            M = (L./q+I)
            xhat = M\y
            rec_err_xhat[i] = norm(xhat - x)
        end
        ## Calculate exact solution with the selected parameter
        q  = q_range[argmin(rec_err_xhat)]
        display("$gname-$sigiter The best hyperparameter q:$q")
        M = (L./q+I)
        xhat = M\y

        avg_rec_err_xhat += minimum(rec_err_xhat)
        display("$gname-$sigiter Reconstruction error of denoised signal = $(minimum(rec_err_xhat))")
        ### Record the results
        q_arr[sigiter] = q
        Xhat[:,sigiter] = xhat


        λs =  l./q .+ 1
        lmin,lmax = minimum(l),maximum(l)
        λmin,λmax = minimum(λs),maximum(λs)
        display("$gname-$sigiter Condition number: $(λmax)")

        display("$gname-$sigiter Backslash ")
        brunnable = @benchmarkable $M\$y
        b = run(brunnable)
        comp_time_backslash += mean(b.times*10^-9)

        ## For mean correction purposes
        ybar = mean(y)
        y = y .- ybar


    ## Polynomial Approximation
        display("$gname-$sigiter Polynomial approximation, lmax:$lmax")
        func_to_approximate = x -> q ./ (x .+ q)


        brunnable = @benchmarkable inverse_with_pol_approx($L, $y, $q, 1; method="none",lmax = $lmax)
        b = run(brunnable)

        for (i,nrep) in enumerate(NiterRange)
            brunnable =  @benchmarkable inverse_with_pol_approx($L, $y, $q, $nrep; method="none",lmax = $lmax)
            b = run(brunnable)
            comp_time_pol[i] += mean(b.times*10^-9)

            x_pol= inverse_with_pol_approx(L, y, q, nrep; method="none",lmax = lmax)
            ### Mean correction
            x_pol = x_pol .- mean(x_pol,dims=1) .+ ybar
            app_err_pol[i] += norm(x_pol - xhat)
            rec_err_pol[i] += norm(x_pol - x)
        end

    ## Conjugate Gradient
        display("$gname-$sigiter CG")

        ### Experiment params

        for (i,nrep) in enumerate(NiterRange)
            ### IMPLEMENTATION BY ITERATIVESOLVERS
            brunnable = @benchmarkable cg( $M, $y; maxiter=$nrep,abstol=-1.0,reltol=-1.0)
            b = run(brunnable)
            comp_time_cg[i] += mean(b.times*10^-9)

            x_cg = cg( M, y; maxiter=nrep,abstol=-1.0,reltol=-1.0)
            ### Mean correction
            x_cg = x_cg .- mean(x_cg,dims=1) .+ ybar
            app_err_cg[i] += norm(x_cg - xhat)
            rec_err_cg[i] += norm(x_cg - x)
        end

        display("$gname-$sigiter Prec CG w/ AMG")

        for (i,nrep) in enumerate(NiterRange)
            ### IMPLEMENTATION BY ITERATIVESOLVERS
            brunnable = @benchmarkable cg($M, $y, Pl = aspreconditioner(ruge_stuben($M)); maxiter=$nrep,abstol=-1.0,reltol=-1.0, log=false)
            b = run(brunnable)
            comp_time_pcg[i] += mean(b.times*10^-9)

            x_pcg =  cg(M, y, Pl = aspreconditioner(ruge_stuben(M)); maxiter=nrep,abstol=-1.0,reltol=-1.0, log=false)
            ### Mean correction
            x_pcg = x_pcg .- mean(x_pcg,dims=1) .+ ybar

            app_err_pcg[i] += norm(x_pcg - xhat)
            rec_err_pcg[i] += norm(x_pcg - x)
        end
        display("$gname-$sigiter Prec CG w/ AMG-$(app_err_pcg)")


    ## RandomForest
        display("$gname-$sigiter rf smoothing bar")

        ### Experiment params
        for (i,nrep) in enumerate(NiterRange)
            brunnable = @benchmarkable smooth_rf_xbar($g,$q,$M,$y;maxiter=$nrep,abstol = 0.0,reltol = 0.0).est
            b = run(brunnable)
            comp_time_rf[i] += mean(b.times*10^-9)

            x_rf = smooth_rf_xbar(g,q,M,y;maxiter=nrep,abstol = 0.0,reltol = 0.0).est
            ### Mean correction
            x_rf = x_rf .- mean(x_rf,dims=1) .+ ybar
            app_err_rf[i] += norm(x_rf - xhat)
            rec_err_rf[i] += norm(x_rf - x)
        end

        display("$gname-$sigiter rf smoothing tilde ")
        global x_rf_tilde = zeros(N)
        for (i,nrep) in enumerate(NiterRange)
            brunnable = @benchmarkable smooth_rf_xtilde($g,$q,$M,$y;maxiter=$nrep,abstol = 0.0,reltol = 0.0).est
            b = run(brunnable)
            comp_time_rf_tilde[i] += mean(b.times*10^-9)

            x_rf_tilde = smooth_rf_xtilde(g,q,M,y;maxiter=nrep,abstol = 0.0,reltol = 0.0).est
            ### Mean correction
            x_rf_tilde = x_rf_tilde .- mean(x_rf_tilde,dims=1) .+ ybar

            app_err_rf_tilde[i] += norm(x_rf_tilde - xhat)
            rec_err_rf_tilde[i] += norm(x_rf_tilde - x)
        end

    end
    comp_time_pol ./= sigRep
    comp_time_cg ./= sigRep
    comp_time_rf ./= sigRep
    comp_time_pcg ./= sigRep
    comp_time_rf_tilde ./= sigRep

    app_err_pol ./= sigRep
    app_err_cg ./= sigRep
    app_err_rf ./= sigRep
    app_err_pcg ./= sigRep
    app_err_rf_tilde ./= sigRep


    rec_err_pol ./= sigRep
    rec_err_cg ./= sigRep
    rec_err_rf ./= sigRep
    rec_err_pcg ./= sigRep
    rec_err_rf_tilde ./= sigRep

    avg_rec_err_xhat /= sigRep
    comp_time_backslash /= sigRep

    filename = string("./outputs/",gname,".jld")
    JLD2.@save(filename,g,q_arr,N,Y,X,Xhat,k,SNR,NiterRange,l,avg_rec_err_xhat,app_err_pol,comp_time_pol,app_err_cg,comp_time_cg,app_err_rf,comp_time_rf,rec_err_pol,rec_err_cg,rec_err_rf,rec_err_pcg,app_err_pcg,comp_time_pcg,rec_err_rf_tilde,app_err_rf_tilde,comp_time_rf_tilde,comp_time_backslash)
end
