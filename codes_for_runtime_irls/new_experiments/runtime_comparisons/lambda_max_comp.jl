using LightGraphs,LinearAlgebra,Statistics, Distances
using KrylovKit,SparseArrays,Random
using PyPlot
using RandomForests
using BenchmarkTools
using Profile
using LinearAlgebra
using NearestNeighbors
using AlgebraicMultigrid
using IterativeSolvers
using JLD2
using PyCall
pygui(true)


include("../../utils.jl")
include("../../ssl_data_loaders.jl")
include("../../cheb_pol_utils.jl")
LinearAlgebra.BLAS.set_num_threads(1)


## CODE STARTS HERE
N = 10000 # number of nodes

nsamples = 10
neval = 10
BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
BenchmarkTools.DEFAULT_PARAMETERS.evals = neval
BenchmarkTools.DEFAULT_PARAMETERS.samples = nsamples


# graphs = ["Pubmed","SBM"]
# graphs = ["Euclidean","Bunny","Cora","Citeseer","Pubmed","SBM"]
graphs = ["Grid","Barabasi-Albert","Erdos-Renyi","K-regular","Euclidean","Bunny","Cora","Citeseer"]

gnum = length(graphs)
sigRep=20
for gname in graphs
    filename = string("./outputs/",gname,".jld")
    JLD2.@load(filename,g,q_arr,N,Y,X,Xhat,k,SNR,NiterRange,l,avg_rec_err_xhat,app_err_pol,comp_time_pol,app_err_cg,comp_time_cg,app_err_rf,comp_time_rf,rec_err_pol,rec_err_cg,rec_err_rf)
    JLD2.@load(filename,N, rec_err_pcg,app_err_pcg,comp_time_pcg)
    JLD2.@load(filename,N, rec_err_rf_tilde,app_err_rf_tilde,comp_time_rf_tilde)
    JLD2.@load(filename,comp_time_backslash)

    comp_time_lambdamax = 0
    L = laplacian_matrix(g) + 10^(-14)*I

    for sigiter = 1 : sigRep
        display("$gname-$sigiter Krylov lmax calculation")

        # for (i,nrep) in enumerate(NiterRange)
        brunnable = @benchmarkable KrylovKit.eigsolve($L, 1, :LM, issymmetric=true);
        b = run(brunnable)
        comp_time_lambdamax+= mean(b.times*10^-9)

        lmax, _, _ = KrylovKit.eigsolve(L, 1, :LM, issymmetric=true); lmax = lmax[1]
        display("$gname-$sigiter Calculated lmax = $lmax,Recorded lmax = $(maximum(l)),")
        # end
        display("$gname-$sigiter Krylov lmax comp. time $comp_time_lambdamax")
    end
    comp_time_lambdamax /= sigRep
    filename = string("./outputs/",gname,".jld")
    JLD2.@save(filename,g,q_arr,N,Y,X,Xhat,k,SNR,NiterRange,l,avg_rec_err_xhat,app_err_pol,comp_time_pol,app_err_cg,comp_time_cg,app_err_rf,comp_time_rf,rec_err_pol,rec_err_cg,rec_err_rf,rec_err_pcg,app_err_pcg,comp_time_pcg,rec_err_rf_tilde,app_err_rf_tilde,comp_time_rf_tilde,comp_time_backslash,comp_time_lambdamax)

end
