
using LightGraphs,LinearAlgebra,Statistics, Distances
using KrylovKit,SparseArrays,Random
using PyPlot
using RandomForests
using BenchmarkTools
using Profile
using LinearAlgebra
using NearestNeighbors
using AlgebraicMultigrid
using IterativeSolvers
using JLD2
using PyCall
pygui(true)


include("../../utils.jl")
include("../../ssl_data_loaders.jl")
include("../../cheb_pol_utils.jl")
LinearAlgebra.BLAS.set_num_threads(1)


## CODE STARTS HERE

nsamples = 10
neval = 10
BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
BenchmarkTools.DEFAULT_PARAMETERS.evals = neval
BenchmarkTools.DEFAULT_PARAMETERS.samples = nsamples

graphs = ["K-regular"]
# graphs = ["Bunny","Cora","Citeseer","Pubmed"]
# graphs = ["path","star"]
gnum = length(graphs)
sigRep=20
for gname in graphs
    filename = string("./outputs/",gname,".jld")
    JLD2.@load(filename,g,q_arr,N,Y,X,Xhat,k,SNR,NiterRange,l,avg_rec_err_xhat,app_err_pol,comp_time_pol,app_err_cg,comp_time_cg,app_err_rf,comp_time_rf,rec_err_pol,rec_err_cg,rec_err_rf)
    JLD2.@load(filename,N, rec_err_pcg,app_err_pcg,comp_time_pcg)
    JLD2.@load(filename,N, rec_err_rf_tilde,app_err_rf_tilde,comp_time_rf_tilde)
    JLD2.@load(filename,comp_time_backslash)
    JLD2.@load(filename,comp_time_lambdamax)

    comp_time_pcg = zeros(length(NiterRange))
    app_err_pcg = zeros(length(NiterRange))
    rec_err_pcg = zeros(length(NiterRange))
    L = laplacian_matrix(g) + 10^(-14)*I
    for sigiter = 1 : sigRep

    ## create a signal to filter
        x = X[:,sigiter]
        y = Y[:,sigiter]
        xhat = Xhat[:,sigiter]
        q = q_arr[sigiter]
        M = (L./q+I)

        display("$gname-$sigiter Initial SNR:$(((norm(x) / norm(y-x)))^2)")
        display("$gname-$sigiter Signal norm:$(norm(x))")
        display("$gname-$sigiter The best hyperparameter q:$q")

        λs =  l./q .+ 1
        lmin,lmax = minimum(l),maximum(l)
        λmin,λmax = minimum(λs),maximum(λs)
        display("$gname-$sigiter Condition number: $(λmax)")

        ## For mean correction purposes
        ybar = mean(y)
        y = y .- ybar
        display("$gname-$sigiter Prec CG w/ AMG")

        if(sigiter < 5 )
            plot(y)
            plot(x)
        end
        display("$gname-$sigiter Prec CG w/ AMG-$(app_err_pcg)")

        for (i,nrep) in enumerate(NiterRange)
            ### IMPLEMENTATION BY ITERATIVESOLVERS

            brunnable = @benchmarkable cg($M, $y, Pl = aspreconditioner(ruge_stuben($M)); maxiter=$nrep,abstol=-1.0,reltol=-1.0, log=false)
            b = run(brunnable)
            comp_time_pcg[i] += mean(b.times*10^-9)

            x_pcg =  cg(M, y, Pl = aspreconditioner(ruge_stuben(M)); maxiter=nrep,abstol=-1.0,reltol=-1.0, log=false)
            ### Mean correction
            x_pcg = x_pcg .- mean(x_pcg,dims=1) .+ ybar

            app_err_pcg[i] += norm(x_pcg - xhat)
            rec_err_pcg[i] += norm(x_pcg - x)
        end
        display("$gname-$sigiter Prec CG w/ AMG-$(app_err_pcg)")
    end
    comp_time_pcg ./= sigRep
    app_err_pcg ./= sigRep
    rec_err_pcg ./= sigRep
    filename = string("./outputs/",gname,".jld")
    JLD2.@save(filename,g,q_arr,N,Y,X,Xhat,k,SNR,NiterRange,l,avg_rec_err_xhat,app_err_pol,comp_time_pol,app_err_cg,comp_time_cg,app_err_rf,comp_time_rf,rec_err_pol,rec_err_cg,rec_err_rf,rec_err_pcg,app_err_pcg,comp_time_pcg,rec_err_rf_tilde,app_err_rf_tilde,comp_time_rf_tilde,comp_time_backslash,comp_time_lambdamax)

end
