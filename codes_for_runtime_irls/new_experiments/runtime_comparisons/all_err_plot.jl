using PyPlot
using JLD2
using LinearAlgebra
using LightGraphs,Statistics, Distances


pygui(false)
rcParams = PyPlot.PyDict(PyPlot.matplotlib."rcParams")
# rcParams["figure.figsize"] = [6,12]

graphs = ["Grid","Barabasi-Albert","Erdos-Renyi","K-regular","Euclidean","Bunny","Cora","Citeseer"]
graphs = ["Citeseer"]
# graphs = ["Bunny","Cora","Citeseer","Pubmed"]
# graphs = ["path","star"]
gnum = length(graphs)
sigRep=2
for gname in graphs
    f=figure(figsize= [10,4])
    filename = string("./outputs/",gname,".jld")
    JLD2.@load(filename,g,q_arr,N,Y,X,Xhat,k,SNR,NiterRange,l,avg_rec_err_xhat,app_err_pol,comp_time_pol,app_err_cg,comp_time_cg,app_err_rf,comp_time_rf,rec_err_pol,rec_err_cg,rec_err_rf)
    JLD2.@load(filename,N, rec_err_pcg,app_err_pcg,comp_time_pcg)
    JLD2.@load(filename,N, rec_err_rf_tilde,app_err_rf_tilde,comp_time_rf_tilde)
    JLD2.@load(filename,comp_time_backslash)
    JLD2.@load(filename,comp_time_lambdamax)

# comp_time_lambdamax =0
    λmin,λmax = minimum(l),maximum(l)
    κ_avg = mean(λmax./q_arr .+ 1)

    # display("$(isnan.(app_err_pol))")
    # display("$(isnan.(app_err_pcg))")
    # display("$(isnan.(app_err_cg))")
    # display("$(isnan.(app_err_rf))")

    a = PyPlot.subplot(1,2,1)

    display("$gname,$(nv(g)),$(ne(g)),$(is_connected(g)),avg_degree:$(mean(degree(g))),q:$(mean(q_arr)),κ_avg:$κ_avg, comp_time_lambdamax:$comp_time_lambdamax")
    # plot(comp_time_rf_tilde,app_err_rf_tilde,linestyle="--",marker="d",color="lime")
    plot(comp_time_pol.+comp_time_lambdamax,app_err_pol,marker=".",color="orange")
    plot(comp_time_cg,app_err_cg,marker="o",color="blue")
    plot(comp_time_pcg,app_err_pcg,marker="o",color="cyan")
    plot(comp_time_rf,app_err_rf,marker="d",color="green")

    xlabel("Time (s)",fontsize=15)
    ylabel("Approximation error",fontsize=15)
    axvline(x=comp_time_backslash,linestyle="--",c="black",label="direct",linewidth=2)
    yscale("log")
    yticks(fontsize=12)
    xscale("log")
    xticks(fontsize=12)

    PyPlot.subplot(1,2,2)
    initial_rec_err = 0.0
    for i = 1 : sigRep
        initial_rec_err += norm(Y[:,i]-X[:,i])
    end
    initial_rec_err /= sigRep



    # plot(comp_time_rf_tilde,rec_err_rf_tilde,label=latexstring("\$\\tilde{x}\$"),linestyle="--",marker="d",color="lime")
    plot(comp_time_pol.+comp_time_lambdamax,rec_err_pol,label="pol",marker=".",color="orange")
    plot(comp_time_cg,rec_err_cg,label="cg",marker="o",color="blue")
    plot(comp_time_pcg,rec_err_pcg,label="pcg",marker="o",color="cyan")
    plot(comp_time_rf,rec_err_rf,label=latexstring("\$\\bar{x}\$"),marker="d",color="green")

    axhline(y=avg_rec_err_xhat,linestyle="--",c="r",label=latexstring("\$|\\hat{x} - x|\$"),linewidth=1)
    axhline(y=initial_rec_err,linestyle="--",c="g",label="|y - x|",linewidth=1)

    axvline(x=comp_time_backslash,linestyle="--",c="black",linewidth=2)
    yscale("log")
    yticks(fontsize=12)
    xscale("log")
    xticks(fontsize=12)
    ylabel("Reconstruction error",fontsize=15)
    xlabel("Time (s)",fontsize=15)

    f.suptitle(string(gname, " Graph") ,fontsize=16)
    figlegend(loc="upper center",ncol = 4,fontsize=20, bbox_to_anchor=(0.55, 0.3),framealpha=1.0)

    f.tight_layout()

    savefig(string("./outputs/",gname,".pdf"))
end
