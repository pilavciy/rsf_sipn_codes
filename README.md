These Julia codes reproduces the results presented in 

Pilavci, Y., Amblard, P. O., Barthelme, S., & Tremblay, N. (2020). Graph Tikhonov Regularization and Interpolation via Random Spanning Forests. arXiv preprint arXiv:2011.10450.

These codes mainly uses the Julia package RandomForests.jl. You can add it in the package manager by typing: 
"add https://gricad-gitlab.univ-grenoble-alpes.fr/barthesi/randomforests.jl"  

For any questions, you may contact with Yusuf Yigit Pilavci via e-mail at firstname.lastname ATT grenoble-inp DOT fr. 
