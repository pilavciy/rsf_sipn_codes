
# get community structure given a few labels (stored in Y)
function ssl(G,Y,mu,sigma;method="exact",nrep=10)
    d = reshape(degree(G),nv(G))
    q = (mu/2) .* d

    D_sm1 = spdiagm(0 => d.^(sigma-1))
    D_1ms = spdiagm(0 => d.^(1-sigma))

    Z = D_sm1 * Y

    if (method=="exact")
        S = D_1ms*smooth(G,q,Z)
    else
        Zbar = repeat(mean(Z, dims=1), outer=size(Z,1))
        Δ = Z - Zbar
        if (method=="xtilde")
            S = D_1ms * (Zbar + smooth_rf(G,q,Δ,[];nrep=nrep,variant=1).est)
        elseif (method=="xbar")
            S = D_1ms * (Zbar + smooth_rf(G,q,Δ,[];nrep=nrep,variant=2).est)
        end
    end
    cluster_ind = zeros(Int64, nv(G))
    k = size(Y,2)
    exaequo = 0
    for i=1:nv(G)
        cluster_ind[i] = argmax(S[i,:])
    end
    return cluster_ind, exaequo
end

function ssl_rootset(G,Y,mu,sigma;method="exact" ,nrep=10,rootset=[])
    q = mu
    k = size(Y,2)
    S = zeros(nv(G),k)
    if (method=="exact")
        L = Matrix(laplacian_matrix(G))
        unlabeled_nodes = setdiff(1:nv(G),rootset)
        Luu = L[unlabeled_nodes,unlabeled_nodes]
        Lul = L[unlabeled_nodes,rootset]
        S[unlabeled_nodes,:] = - Luu \ (Lul*Y[rootset,:])
        S[rootset,:] = Y[rootset,:]
    else
        Ybar = repeat(mean(Y, dims=1), outer=size(Y,1))
        Δ = Y - Ybar
        if (method=="xtilde")
            S = (Ybar + smooth_rf(G,q,Δ,rootset;nrep=nrep,variant=1).est)
        elseif (method=="xbar")
            S =  (Ybar + smooth_rf(G,q,Δ,rootset;nrep=nrep,variant=2).est)
        end
    end
    cluster_ind = zeros(Int64, nv(G))
    exaequo = 0
    for i=1:nv(G)
        cluster_ind[i] = argmax(S[i,:])
    end
    return cluster_ind, S
end

function sample_labels(labels,m)
    numofclass = size(labels,2)
    n = size(labels,1)
    Y = zeros(n,numofclass)
    labeled_nodes = []
    for i = 1 : numofclass
        labeled_nodes_per_class = StatsBase.sample(findall(labels[:,i] .== 1), m; replace=false)
        Y[labeled_nodes_per_class,i] .= 1
        labeled_nodes = vcat(labeled_nodes,labeled_nodes_per_class)
    end
    return Y,labeled_nodes
end

function encode_labels(labels,numofclass)
    classlist = unique(labels)
    Y = zeros(length(labels),numofclass)
    for i in classlist
        labeled_nodes_per_class = findall(labels .== i)
        Y[labeled_nodes_per_class,i] .= 1
    end
    return Y
end

# get community structure given a few labels (stored in Y)
function ssl_scores(G,Y,mu,labeled_nodes,sigma;method="exact",nrep=10)
    #has_label = sum(Y,dims=1) .!= 0
    d = reshape(degree(G),nv(G))
    q = (mu/2) .* d

    D_sm1 = spdiagm(0 => d.^(sigma-1))
    D_1ms = spdiagm(0 => d.^(1-sigma))

    Z = D_sm1 * Y
    Id = spdiagm(0 => ones(nv(G)))
    Id = Id[:,labeled_nodes]

    if (method=="exact")
        S = D_1ms*smooth(G,q,Z)
        kii = diag(smooth(G,q,Id)[labeled_nodes,:])
    else
        Zbar = repeat(mean(Z, dims=1), outer=size(Y,1))
        Δ = Z - Zbar
        if (method=="xtilde")
            est,nroot,kii = smooth_rf(G,q,Δ,[];nrep=nrep,variant=1)
            S = D_1ms * (Zbar + est)
        elseif (method=="xbar")
            est,nroot,kii = smooth_rf(G,q,Δ,[];nrep=nrep,variant=2)
            S = D_1ms * (Zbar + est)
        end
        kii = kii[labeled_nodes]
    end
    return S,kii
end

function ssl_LOOCV(G,Y,labeled_nodes;sigma=0.0,m=5,mu_range=0.1:0.1:1.0,nrep = 10,method="xbar")
    min_score = typemax(Float64)
    minmu = 0.0
    score =zeros(length(mu_range))
    for (i,mmu) in enumerate(mu_range)
        f,kii = (ssl_scores(G,Y,mmu,labeled_nodes,sigma,method=method,nrep=nrep))
        score[i] = mean(((f[labeled_nodes,:] .- Y[labeled_nodes,:])./(1 .- kii .+eps(Float32))).^2)

        if(min_score > score[i])
            min_score = score[i]
            minmu = mmu
        end
    end
    return minmu,score
end

