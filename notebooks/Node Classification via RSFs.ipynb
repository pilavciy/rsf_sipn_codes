{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Node Classification via RSFs \n",
    "\n",
    "In this notebook, the RSF based methods are illustrated in the context of node classification problem. \n",
    "\n",
    "$\\underline{\\text{Problem}}$ : Given a graph $\\mathcal{G}=(\\mathcal{V},\\mathcal{E},w)$ and a few labels on a vertex subset $\\ell\\subset\\mathcal{V}$, node classification task is to infer the labels or classes of the other vertices $u=\\mathcal{V}\\setminus\\ell$. \n",
    "\n",
    "$\\underline{\\text{Label Encoding}}$ : To able to use the priory information in the algorithms, the known labels are interpreted by the following binary encoding for each class $c$: \n",
    "\n",
    "$$\n",
    "\\hspace{5cm} \\forall i \\in \\mathcal{V}, \\quad \\mathbf{y}_c(i) = \\begin{cases}\n",
    "1 \\text{ if } i \\text{ is known to belong to the } c\\text{-th class} \\\\ \n",
    "0 \\text{ otherwise}\n",
    "\\end{cases}\n",
    "$$ \n",
    "which defines the columns of the encoding matrix $\\mathsf{Y}=[\\mathbf{y}_1,\\mathbf{y}_2,...,\\mathbf{y}_c]\\in\\mathbb{R}^{|\\mathcal{V}|\\times C}$ for $C$ classes.\n",
    "\n",
    "\n",
    "$\\underline{\\text{Solutions}}$ : Given this encoding, two well-known baseline approaches to solve the problem are: \n",
    "- Label Propagatation ([Zhu et.al.](http://pages.cs.wisc.edu/~jerryzhu/pub/thesis.pdf))\n",
    "- Generalized Optimization Framework for Graph-based Semi-supervised Learning ([Avrachenkov et.al.](https://arxiv.org/abs/1110.4278))\n",
    "\n",
    "In the following, we illustrate the RSF versions of these algorithms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "┌ Info: Precompiling MLJ [add582a8-e3ab-11e8-2d5e-e98b27df1bc7]\n",
      "└ @ Base loading.jl:1260\n",
      "[ Info: Model metadata loaded from registry. \n"
     ]
    }
   ],
   "source": [
    "### PACKAGES AND FUNCTIONS ### \n",
    "using RandomForests\n",
    "using LightGraphs\n",
    "using GraphIO\n",
    "using Plots\n",
    "using Plots.PlotMeasures\n",
    "using StatsBase\n",
    "using Clustering\n",
    "using SparseArrays\n",
    "using LinearAlgebra\n",
    "using MLJ\n",
    "using JLD2\n",
    "using LaTeXStrings\n",
    "using NamedArrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "┌ Warning: ORCA.jl has been deprecated and all savefig functionality\n",
      "│ has been implemented directly in PlotlyBase itself.\n",
      "│ \n",
      "│ By implementing in PlotlyBase.jl, the savefig routines are automatically\n",
      "│ available to PlotlyJS.jl also.\n",
      "└ @ ORCA /home/pilavciy-admin/.julia/packages/ORCA/U5XaN/src/ORCA.jl:8\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "load_citeseer (generic function with 1 method)"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "plotly()\n",
    "include(\"utils.jl\")\n",
    "include(\"ssl_data_loaders.jl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load the benchmark dataset (Graph and ground truth labels)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "G , gr_truth = load_citeseer();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(CategoricalArrays.CategoricalValue{Int64,UInt32} 2, [0.0, 1.0, 0.0, 0.0, 0.0, 0.0])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "## Generate the encoding given the labels. \n",
    "gr_truth = categorical(gr_truth)\n",
    "classnum = length(unique(gr_truth))\n",
    "encoded_labels = encode_labels(gr_truth,classnum)\n",
    "\n",
    "# The label of 10th vertex and the corresponding encoding\n",
    "gr_truth[10],encoded_labels[10,:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Priory labels are generated "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = 30\n",
    "Y,labeled_nodes = sample_labels(encoded_labels,m)\n",
    "unlabeled_nodes = setdiff(1:nv(G),labeled_nodes);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Label Propagation \n",
    "\n",
    "Label propagation is a simple algorithm that follows the following procedure:\n",
    " 1. Initialize $t=0$, $\\mathsf{Y}^{(t)}=\\mathsf{Y}$ and $P = \\mathsf{D}^{-1}\\mathsf{W} \\in \\mathbb{R}^{|\\mathcal{V}| \\times |\\mathcal{V}|}$ where $\\mathsf{W}$ is the (weighted) adjacency matrix and $\\mathsf{D}$ is the diagonal degree matrix\n",
    " 2. Compute $\\mathsf{Y}^{(t+1)} \\leftarrow \\mathsf{P}\\mathsf{Y}^{(t)}$\n",
    " 3. Set $\\mathsf{Y}^{(t+1)}_{\\ell|:} = \\mathsf{Y}_{\\ell|:}$\n",
    " 4. Go back to step 2 if $\\mathsf{Y}^{(t+1)}$ is not converged. \n",
    " \n",
    "In the end of this procedure, $\\mathsf{Y}^{(t+1)}$ converges to the following for each class $c$: \n",
    "$$\n",
    "\\mathsf{F}_{i,c} = \\begin{cases}\n",
    "\\mathsf{Y}_{i,c}, &\\text{ if } i \\in \\ell  \\\\ \n",
    "(-(\\mathsf{L}_{u|u})^{-1}\\mathsf{L}_{u|\\ell}\\mathsf{Y}_{\\ell|:})_{i,c}, &\\text{ otherwise }   \\\\ \n",
    "\\end{cases}\n",
    "$$\n",
    "In the termination of the algorithm, the label of every node $i\\in u$ is determined by $\\arg\\min_{c}\\mathsf{F}_{i,c}$\n",
    "\n",
    "### Label propagation with RSFs. \n",
    "- It is also possible to approximate this solution by using random spanning forests. \n",
    "- Consider random spanning forests with the pre-determined rootset $\\ell$. \n",
    "- This means the loop erased random walks in Wilson's algorithm only stops at the nodes in $\\ell$. \n",
    "- With this setup, the proposed estimator $\\tilde{\\mathbf{x}}$ becomes an unbiased estimator of $\\mathsf{F}$ given the input vectors $\\mathsf{Y}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "nrep = 20\n",
    "rootset = labeled_nodes\n",
    "est_cl_hat_lp,_ = (ssl_rootset(G,Y,0.0,0.0,method=\"exact\",nrep=nrep,rootset=rootset))\n",
    "est_cl_tilde_lp,_ = (ssl_rootset(G,Y,0.0,0.0,method=\"xtilde\",nrep=nrep,rootset=rootset));\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generalized Optimization Framework for Graph-based Semi-supervised Learning\n",
    "\n",
    "The optimization framework proposed in [Avrachenkov et. al.]() admits the following solution: \n",
    "$$\n",
    "\\mathsf{F} = \\frac{\\mu}{\\mu+2}\\left(\\mathsf{I} - \\frac{2}{\\mu+2}\\mathsf{D}^{-\\sigma} \\mathsf{W}\\mathsf{D}^{\\sigma-1}\\right)^{-1}\\mathsf{Y}\n",
    "$$\n",
    "where $\\mu$ is the regularization parameter and $\\sigma$ choses the regularizer Laplacian. This can be also written as: \n",
    "\n",
    "$$ \n",
    "\\mathsf{F} = \\mathsf{D}^{1-\\sigma }\\mathsf{K} \\mathsf{D}^{\\sigma -1}\\mathsf{Y} \\text{ with } \\mathsf{K} = (\\mathsf{L} + \\mathsf{Q})^{-1}\\mathsf{Q}\n",
    "$$\n",
    "where $\\mathsf{Q} = \\frac{\\mu}{2}\\mathsf{D}$. \n",
    "\n",
    "### RSF approximation. \n",
    "- The computationally expensive part is the product $\\mathsf{K} \\mathsf{D}^{\\sigma -1}\\mathsf{Y}$ \n",
    "- It can be approximated by the proposed estimators with the input vector $\\mathsf{Y}= \\mathsf{D}^{\\sigma -1}\\mathsf{Y}$ and the $q$ values $\\mathsf{Q} = \\frac{\\mu}{2}\\mathsf{D}$.\n",
    "- Moreover, $\\mu$ can be automatically selected to get the best classification performance "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hyperparameter ($\\mu$) selection\n",
    "$\\mu$ is automatically selected using leave-one-out cross validation for each method\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "    <script type=\"text/javascript\">\n",
       "        requirejs([\"https://cdn.plot.ly/plotly-1.54.2.min.js\"], function(p) {\n",
       "            window.Plotly = p\n",
       "        });\n",
       "    </script>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.plotly.v1+json": {
       "data": [
        {
         "colorbar": {
          "title": ""
         },
         "legendgroup": "LOOCV Score of ̂x",
         "line": {
          "color": "rgba(0, 154, 250, 1.000)",
          "dash": "solid",
          "shape": "linear",
          "width": 6
         },
         "mode": "lines",
         "name": "LOOCV Score of ̂x",
         "showlegend": true,
         "type": "scatter",
         "x": [
          0.01,
          0.1,
          1,
          10
         ],
         "xaxis": "x1",
         "y": [
          0.1590406080968813,
          0.1540068524301498,
          0.15143354282865537,
          0.15282317744199203
         ],
         "yaxis": "y1",
         "zmax": null,
         "zmin": null
        },
        {
         "colorbar": {
          "title": ""
         },
         "legendgroup": "LOOCV Score of ̃x",
         "line": {
          "color": "rgba(227, 111, 71, 1.000)",
          "dash": "solid",
          "shape": "linear",
          "width": 6
         },
         "mode": "lines",
         "name": "LOOCV Score of ̃x",
         "showlegend": true,
         "type": "scatter",
         "x": [
          0.01,
          0.1,
          1,
          10
         ],
         "xaxis": "x1",
         "y": [
          0.1623370372933882,
          0.15603213623218337,
          0.1544064737406953,
          0.15896904005657592
         ],
         "yaxis": "y1",
         "zmax": null,
         "zmin": null
        },
        {
         "colorbar": {
          "title": ""
         },
         "legendgroup": "LOOCV Score of ̄x",
         "line": {
          "color": "rgba(62, 164, 78, 1.000)",
          "dash": "solid",
          "shape": "linear",
          "width": 6
         },
         "mode": "lines",
         "name": "LOOCV Score of ̄x",
         "showlegend": true,
         "type": "scatter",
         "x": [
          0.01,
          0.1,
          1,
          10
         ],
         "xaxis": "x1",
         "y": [
          0.1587614724545267,
          0.15369473876683082,
          0.1522001242489064,
          0.15423306408996443
         ],
         "yaxis": "y1",
         "zmax": null,
         "zmin": null
        }
       ],
       "layout": {
        "annotations": [],
        "height": 500,
        "legend": {
         "bgcolor": "rgba(255, 255, 255, 1.000)",
         "bordercolor": "rgba(0, 0, 0, 1.000)",
         "borderwidth": 1,
         "font": {
          "color": "rgba(0, 0, 0, 1.000)",
          "family": "sans-serif",
          "size": 21
         },
         "tracegroupgap": 0,
         "traceorder": "normal",
         "x": 1.05,
         "xanchor": "upper",
         "y": 1,
         "yanchor": "right"
        },
        "margin": {
         "b": 20,
         "l": 0,
         "r": 0,
         "t": 20
        },
        "paper_bgcolor": "rgba(255, 255, 255, 1.000)",
        "plot_bgcolor": "rgba(255, 255, 255, 1.000)",
        "showlegend": true,
        "width": 800,
        "xaxis": {
         "anchor": "y1",
         "domain": [
          0.29981408573928264,
          0.9950787401574803
         ],
         "gridcolor": "rgba(0, 0, 0, 0.100)",
         "gridwidth": 0.5,
         "linecolor": "rgba(0, 0, 0, 1.000)",
         "mirror": false,
         "range": [
          -0.2897,
          10.2997
         ],
         "showgrid": true,
         "showline": true,
         "showticklabels": true,
         "tickangle": 0,
         "tickcolor": "rgb(0, 0, 0)",
         "tickfont": {
          "color": "rgba(0, 0, 0, 1.000)",
          "family": "sans-serif",
          "size": 28
         },
         "tickmode": "array",
         "ticks": "inside",
         "ticktext": [
          "0.0",
          "2.5",
          "5.0",
          "7.5",
          "10.0"
         ],
         "tickvals": [
          0,
          2.5,
          5,
          7.5,
          10
         ],
         "title": "mu",
         "titlefont": {
          "color": "rgba(0, 0, 0, 1.000)",
          "family": "sans-serif",
          "size": 28
         },
         "type": "-",
         "visible": true,
         "zeroline": false,
         "zerolinecolor": "rgba(0, 0, 0, 1.000)"
        },
        "yaxis": {
         "anchor": "x1",
         "domain": [
          0.11898512685914263,
          0.9921259842519685
         ],
         "gridcolor": "rgba(0, 0, 0, 0.100)",
         "gridwidth": 0.5,
         "linecolor": "rgba(0, 0, 0, 1.000)",
         "mirror": false,
         "range": [
          0.15110643799471338,
          0.16266414212733019
         ],
         "showgrid": true,
         "showline": true,
         "showticklabels": true,
         "tickangle": 0,
         "tickcolor": "rgb(0, 0, 0)",
         "tickfont": {
          "color": "rgba(0, 0, 0, 1.000)",
          "family": "sans-serif",
          "size": 28
         },
         "tickmode": "array",
         "ticks": "inside",
         "ticktext": [
          "0.1525",
          "0.1550",
          "0.1575",
          "0.1600",
          "0.1625"
         ],
         "tickvals": [
          0.1525,
          0.155,
          0.1575,
          0.16,
          0.1625
         ],
         "title": "LOOCV Score",
         "titlefont": {
          "color": "rgba(0, 0, 0, 1.000)",
          "family": "sans-serif",
          "size": 28
         },
         "type": "-",
         "visible": true,
         "zeroline": false,
         "zerolinecolor": "rgba(0, 0, 0, 1.000)"
        }
       }
      },
      "text/html": [
       "<!DOCTYPE html>\n",
       "<html>\n",
       "    <head>\n",
       "        <title>Plots.jl</title>\n",
       "        <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\n",
       "        <script src=\"https://cdn.plot.ly/plotly-1.54.2.min.js\"></script>\n",
       "    </head>\n",
       "    <body>\n",
       "            <div id=\"b7b3f5e6-80cf-4ca1-ac02-cef4a7e3229f\" style=\"width:800px;height:500px;\"></div>\n",
       "    <script>\n",
       "    PLOT = document.getElementById('b7b3f5e6-80cf-4ca1-ac02-cef4a7e3229f');\n",
       "    Plotly.plot(PLOT, [\n",
       "    {\n",
       "        \"xaxis\": \"x1\",\n",
       "        \"colorbar\": {\n",
       "            \"title\": \"\"\n",
       "        },\n",
       "        \"yaxis\": \"y1\",\n",
       "        \"x\": [\n",
       "            0.01,\n",
       "            0.1,\n",
       "            1.0,\n",
       "            10.0\n",
       "        ],\n",
       "        \"showlegend\": true,\n",
       "        \"mode\": \"lines\",\n",
       "        \"name\": \"LOOCV Score of ̂x\",\n",
       "        \"zmin\": null,\n",
       "        \"legendgroup\": \"LOOCV Score of ̂x\",\n",
       "        \"zmax\": null,\n",
       "        \"line\": {\n",
       "            \"color\": \"rgba(0, 154, 250, 1.000)\",\n",
       "            \"shape\": \"linear\",\n",
       "            \"dash\": \"solid\",\n",
       "            \"width\": 6\n",
       "        },\n",
       "        \"y\": [\n",
       "            0.1590406080968813,\n",
       "            0.1540068524301498,\n",
       "            0.15143354282865537,\n",
       "            0.15282317744199203\n",
       "        ],\n",
       "        \"type\": \"scatter\"\n",
       "    },\n",
       "    {\n",
       "        \"xaxis\": \"x1\",\n",
       "        \"colorbar\": {\n",
       "            \"title\": \"\"\n",
       "        },\n",
       "        \"yaxis\": \"y1\",\n",
       "        \"x\": [\n",
       "            0.01,\n",
       "            0.1,\n",
       "            1.0,\n",
       "            10.0\n",
       "        ],\n",
       "        \"showlegend\": true,\n",
       "        \"mode\": \"lines\",\n",
       "        \"name\": \"LOOCV Score of ̃x\",\n",
       "        \"zmin\": null,\n",
       "        \"legendgroup\": \"LOOCV Score of ̃x\",\n",
       "        \"zmax\": null,\n",
       "        \"line\": {\n",
       "            \"color\": \"rgba(227, 111, 71, 1.000)\",\n",
       "            \"shape\": \"linear\",\n",
       "            \"dash\": \"solid\",\n",
       "            \"width\": 6\n",
       "        },\n",
       "        \"y\": [\n",
       "            0.1623370372933882,\n",
       "            0.15603213623218337,\n",
       "            0.1544064737406953,\n",
       "            0.15896904005657592\n",
       "        ],\n",
       "        \"type\": \"scatter\"\n",
       "    },\n",
       "    {\n",
       "        \"xaxis\": \"x1\",\n",
       "        \"colorbar\": {\n",
       "            \"title\": \"\"\n",
       "        },\n",
       "        \"yaxis\": \"y1\",\n",
       "        \"x\": [\n",
       "            0.01,\n",
       "            0.1,\n",
       "            1.0,\n",
       "            10.0\n",
       "        ],\n",
       "        \"showlegend\": true,\n",
       "        \"mode\": \"lines\",\n",
       "        \"name\": \"LOOCV Score of ̄x\",\n",
       "        \"zmin\": null,\n",
       "        \"legendgroup\": \"LOOCV Score of ̄x\",\n",
       "        \"zmax\": null,\n",
       "        \"line\": {\n",
       "            \"color\": \"rgba(62, 164, 78, 1.000)\",\n",
       "            \"shape\": \"linear\",\n",
       "            \"dash\": \"solid\",\n",
       "            \"width\": 6\n",
       "        },\n",
       "        \"y\": [\n",
       "            0.1587614724545267,\n",
       "            0.15369473876683082,\n",
       "            0.1522001242489064,\n",
       "            0.15423306408996443\n",
       "        ],\n",
       "        \"type\": \"scatter\"\n",
       "    }\n",
       "]\n",
       ", {\n",
       "    \"showlegend\": true,\n",
       "    \"xaxis\": {\n",
       "        \"showticklabels\": true,\n",
       "        \"gridwidth\": 0.5,\n",
       "        \"tickvals\": [\n",
       "            0.0,\n",
       "            2.5,\n",
       "            5.0,\n",
       "            7.5,\n",
       "            10.0\n",
       "        ],\n",
       "        \"visible\": true,\n",
       "        \"ticks\": \"inside\",\n",
       "        \"range\": [\n",
       "            -0.2897,\n",
       "            10.2997\n",
       "        ],\n",
       "        \"domain\": [\n",
       "            0.29981408573928264,\n",
       "            0.9950787401574803\n",
       "        ],\n",
       "        \"tickmode\": \"array\",\n",
       "        \"linecolor\": \"rgba(0, 0, 0, 1.000)\",\n",
       "        \"showgrid\": true,\n",
       "        \"title\": \"mu\",\n",
       "        \"mirror\": false,\n",
       "        \"tickangle\": 0,\n",
       "        \"showline\": true,\n",
       "        \"gridcolor\": \"rgba(0, 0, 0, 0.100)\",\n",
       "        \"titlefont\": {\n",
       "            \"color\": \"rgba(0, 0, 0, 1.000)\",\n",
       "            \"family\": \"sans-serif\",\n",
       "            \"size\": 28\n",
       "        },\n",
       "        \"tickcolor\": \"rgb(0, 0, 0)\",\n",
       "        \"ticktext\": [\n",
       "            \"0.0\",\n",
       "            \"2.5\",\n",
       "            \"5.0\",\n",
       "            \"7.5\",\n",
       "            \"10.0\"\n",
       "        ],\n",
       "        \"zeroline\": false,\n",
       "        \"type\": \"-\",\n",
       "        \"tickfont\": {\n",
       "            \"color\": \"rgba(0, 0, 0, 1.000)\",\n",
       "            \"family\": \"sans-serif\",\n",
       "            \"size\": 28\n",
       "        },\n",
       "        \"zerolinecolor\": \"rgba(0, 0, 0, 1.000)\",\n",
       "        \"anchor\": \"y1\"\n",
       "    },\n",
       "    \"paper_bgcolor\": \"rgba(255, 255, 255, 1.000)\",\n",
       "    \"annotations\": [],\n",
       "    \"height\": 500,\n",
       "    \"margin\": {\n",
       "        \"l\": 0,\n",
       "        \"b\": 20,\n",
       "        \"r\": 0,\n",
       "        \"t\": 20\n",
       "    },\n",
       "    \"plot_bgcolor\": \"rgba(255, 255, 255, 1.000)\",\n",
       "    \"yaxis\": {\n",
       "        \"showticklabels\": true,\n",
       "        \"gridwidth\": 0.5,\n",
       "        \"tickvals\": [\n",
       "            0.1525,\n",
       "            0.155,\n",
       "            0.1575,\n",
       "            0.16,\n",
       "            0.1625\n",
       "        ],\n",
       "        \"visible\": true,\n",
       "        \"ticks\": \"inside\",\n",
       "        \"range\": [\n",
       "            0.15110643799471338,\n",
       "            0.16266414212733019\n",
       "        ],\n",
       "        \"domain\": [\n",
       "            0.11898512685914263,\n",
       "            0.9921259842519685\n",
       "        ],\n",
       "        \"tickmode\": \"array\",\n",
       "        \"linecolor\": \"rgba(0, 0, 0, 1.000)\",\n",
       "        \"showgrid\": true,\n",
       "        \"title\": \"LOOCV Score\",\n",
       "        \"mirror\": false,\n",
       "        \"tickangle\": 0,\n",
       "        \"showline\": true,\n",
       "        \"gridcolor\": \"rgba(0, 0, 0, 0.100)\",\n",
       "        \"titlefont\": {\n",
       "            \"color\": \"rgba(0, 0, 0, 1.000)\",\n",
       "            \"family\": \"sans-serif\",\n",
       "            \"size\": 28\n",
       "        },\n",
       "        \"tickcolor\": \"rgb(0, 0, 0)\",\n",
       "        \"ticktext\": [\n",
       "            \"0.1525\",\n",
       "            \"0.1550\",\n",
       "            \"0.1575\",\n",
       "            \"0.1600\",\n",
       "            \"0.1625\"\n",
       "        ],\n",
       "        \"zeroline\": false,\n",
       "        \"type\": \"-\",\n",
       "        \"tickfont\": {\n",
       "            \"color\": \"rgba(0, 0, 0, 1.000)\",\n",
       "            \"family\": \"sans-serif\",\n",
       "            \"size\": 28\n",
       "        },\n",
       "        \"zerolinecolor\": \"rgba(0, 0, 0, 1.000)\",\n",
       "        \"anchor\": \"x1\"\n",
       "    },\n",
       "    \"legend\": {\n",
       "        \"yanchor\": \"right\",\n",
       "        \"xanchor\": \"upper\",\n",
       "        \"bordercolor\": \"rgba(0, 0, 0, 1.000)\",\n",
       "        \"bgcolor\": \"rgba(255, 255, 255, 1.000)\",\n",
       "        \"font\": {\n",
       "            \"color\": \"rgba(0, 0, 0, 1.000)\",\n",
       "            \"family\": \"sans-serif\",\n",
       "            \"size\": 21\n",
       "        },\n",
       "        \"tracegroupgap\": 0,\n",
       "        \"y\": 1.0,\n",
       "        \"borderwidth\": 1,\n",
       "        \"traceorder\": \"normal\",\n",
       "        \"x\": 1.05\n",
       "    },\n",
       "    \"width\": 800\n",
       "}\n",
       ");\n",
       "    </script>\n",
       "\n",
       "    </body>\n",
       "</html>\n"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "loocv_nrep = 30\n",
    "MU = [0.01,0.1,1.0,10.0]\n",
    "mu_hat,err_hat = ssl_LOOCV(G,Y,labeled_nodes,mu_range=MU,method=\"exact\")\n",
    "mu_tilde,err_tilde = ssl_LOOCV(G,Y,labeled_nodes,mu_range=MU,nrep=loocv_nrep,method=\"xtilde\")\n",
    "mu_bar,err_bar = ssl_LOOCV(G,Y,labeled_nodes,mu_range=MU,nrep=loocv_nrep,method=\"xbar\"); \n",
    "\n",
    "\n",
    "plot(MU,err_hat,line=(:solid,6),label=\"LOOCV Score of ̂x\")\n",
    "plot!(MU,err_tilde,line=(:solid,6),label=\"LOOCV Score of ̃x\")\n",
    "plot!(MU,err_bar,line=(:solid,6),label=\"LOOCV Score of ̄x\")\n",
    "plot!(xlabel = \"mu\", ylabel = \"LOOCV Score\")\n",
    "\n",
    "plot!(size=[800,500],tickfont=20, guidefont=20,legendfont=15,legend=:outertopright,leftmargin=20mm)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Estimation of the results via RSFs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "nrep = 200\n",
    "sigma = 0.0\n",
    "est_cl_hat, _ = ssl(G,Y,mu_hat,sigma,method=\"exact\",nrep=nrep)\n",
    "est_cl_tilde, _ = ssl(G,Y,mu_tilde,sigma,method=\"xtilde\",nrep=nrep)\n",
    "est_cl_bar, _ = ssl(G,Y,mu_bar,sigma,method=\"xbar\",nrep=nrep);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×3 Named Array{Float64,2}\n",
       "Solution ╲ Estimator │     xhat    xtilde      xbar\n",
       "─────────────────────┼─────────────────────────────\n",
       "LP                   │ 0.710363  0.706218       NaN\n",
       "gSSL                 │ 0.662176  0.566839  0.656477"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Classification accuracy of LP, g-SSL and their RSF versions\n"
     ]
    }
   ],
   "source": [
    "## Calculate the classification accuracy of the results\n",
    "acc = zeros(2,3)\n",
    "acc[1,1] =  accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_hat_lp[unlabeled_nodes]))[1]\n",
    "acc[1,2] =  accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_tilde_lp[unlabeled_nodes]))[1]\n",
    "acc[1,3] =  NaN\n",
    "acc[2,1] =  accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_hat[unlabeled_nodes]))[1]\n",
    "acc[2,2] =  accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_tilde[unlabeled_nodes]))[1]\n",
    "acc[2,3] =  accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_bar[unlabeled_nodes]))[1];\n",
    "acc =NamedArray(acc)\n",
    "setnames!(acc, [\"LP\",\"gSSL\"], 1)\n",
    "setnames!(acc, [\"xhat\",\"xtilde\",\"xbar\"], 2);\n",
    "setdimnames!(acc, \"Solution\", 1)\n",
    "setdimnames!(acc, \"Estimator\", 2)\n",
    "println(\"Classification accuracy of LP, g-SSL and their RSF versions\")\n",
    "display(acc)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.4.1",
   "language": "julia",
   "name": "julia-1.4"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.4.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
