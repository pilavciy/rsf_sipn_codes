using RandomForests
using LightGraphs,SimpleWeightedGraphs
using GraphIO
using BenchmarkTools
using SparseArrays
using LinearAlgebra
using MLJ
using CSV
using NPZ
using DelimitedFiles
using JLD2
using LaTeXStrings

include("utils.jl")
include("ssl_data_loaders.jl")

# graphs = ["grid100","grid50","10regular104","10regular103","erdosrenyi104","erdosrenyi103","Citeseer","Pubmed"]
graphs = ["grid100","10regular104","erdosrenyi104","Citeseer","Pubmed"]
qrange = [10^y for y in range(log10(10^-3), log10(100.0), length=10)]


rf_times = Array{BenchmarkTools.Trial}(undef,length(graphs),length(qrange))
Lx_times = Array{BenchmarkTools.Trial}(undef,length(graphs))

nsamples = 100
neval = 100
BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
BenchmarkTools.DEFAULT_PARAMETERS.evals = neval
BenchmarkTools.DEFAULT_PARAMETERS.samples = nsamples
outputfolder = string("output/runtime/")
println("Output Folder: $outputfolder")
try
    mkdir(outputfolder)
catch
    println("$outputfolder already exists")
end
for (i,g) in enumerate(graphs)
    if(g == "grid100")
        G = LightGraphs.SimpleGraphs.grid([100,100])
    elseif(g == "grid50")
        G = LightGraphs.SimpleGraphs.grid([50,50])
    elseif(g == "10regular104")
        G = LightGraphs.SimpleGraphs.random_regular_graph(10^4,10)
    elseif(g == "10regular103")
        G = LightGraphs.SimpleGraphs.random_regular_graph(10^3,10)
    elseif(g == "erdosrenyi104")
        G = LightGraphs.SimpleGraphs.erdos_renyi(10^4,0.001)
    elseif(g == "erdosrenyi103")
        G = LightGraphs.SimpleGraphs.erdos_renyi(10^3,0.01)
    elseif(g == "Citeseer")
        G,_ = load_citeseer()
    elseif(g == "Pubmed")
        G,_ = load_PubMed()
    end
    println("Graph: $g")
    println("#nodes = $(nv(G)), #edges = $(ne(G))")
    println("Average degree: $(mean(degree(G)))")

    y = rand(nv(G));
    L = laplacian_matrix(G)

    for (j,q) in enumerate(qrange)
        println("Graph: $g, q value: $q")
        b = @benchmarkable random_forest($G,$q);
        runb = run(b)
        rf_times[i,j] = runb
    end

    bench_Lx = @benchmarkable $L*$y;
    Lx_times[i] = run(bench_Lx)
end
@save string(outputfolder,"workspace.jld2") graphs qrange rf_times Lx_times
