using RandomForests
using LightGraphs,SimpleWeightedGraphs
using ParserCombinator
using GraphIO
using StatsBase
using BenchmarkTools
using SparseArrays
using LinearAlgebra
using MLJ
using CSV
using NPZ
using JLD2

include("utils.jl")
include("ssl_data_loaders.jl")

## Benchmark parameters
nsamples = 100
neval = 1
BenchmarkTools.DEFAULT_PARAMETERS.seconds = Inf
BenchmarkTools.DEFAULT_PARAMETERS.evals = neval
BenchmarkTools.DEFAULT_PARAMETERS.samples = nsamples

## Load the dataset
G , gr_truth = load_PubMed()

gr_truth = categorical(gr_truth)
classnum = length(unique(gr_truth))
encoded_labels = encode_labels(gr_truth,classnum)

## Choose the mu parameter for TR
MU = [0.01,0.1,1.0,10.0]
nrep = 100
loocv_nrep = 50
m = 20
n = nv(G)
d = reshape(degree(G),nv(G))
Y,labeled_nodes = sample_labels(encoded_labels,m)
unlabeled_nodes = setdiff(1:nv(G),labeled_nodes)
mu_rf,err_rf = ssl_LOOCV(G,Y,labeled_nodes,mu_range=MU,nrep=loocv_nrep,method="xbar")
mu = copy(mu_rf)

### TIKHONOV REGULARIZATION ###
q = (mu/2) .* d
time_tr = @benchmarkable random_forest($G,$q,$[])
display(run(time_tr))
### LABEL PROPAGATION ###
rootset = labeled_nodes
time_lp = @benchmarkable random_forest($G,$(0.0),$rootset)
display(run(time_lp))
