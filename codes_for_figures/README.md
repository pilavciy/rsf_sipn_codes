This is the Julia code reproducing the results detailed in

    Yusuf Y. Pilavci, P.-O. Amblard, S. Barthelmé, N. Tremblay.

The authors of this code are Yusuf Yigit Pilavci, Nicolas Tremblay and Simon Barthelmé and can be joined by email at firstname.lastname ATT grenoble-inp DOT fr

If you use this code, please kindly cite us! :-)

========= NOTA BENE BEFORE YOU USE THE CODE =========

* This code uses the Julia RandomForests.jl toolbox (that we wrote for the occasion). You need to install the RandomForests package by typing (in the pkg manager):
'''add https://gricad-gitlab.univ-grenoble-alpes.fr/barthesi/randomforests.jl'''
