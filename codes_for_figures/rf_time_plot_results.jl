using JLD2
using PyPlot
using LaTeXStrings
using MLJ
using ColorSchemes
using BenchmarkTools

outputfolder = string("output/runtime/")
JLD2.@load string(outputfolder,"workspace.jld2") graphs qrange rf_times Lx_times
fig = figure(figsize=[12,6])
g = 1
subplot(1,2,1)
Lx_time_mean = mean(Lx_times[g]).time*10^-6
plot(qrange,[x.time for x in mean.(rf_times[g,:])]*10^-6,color="royalblue", linewidth=6.0,label="2Dgrid")
plot(qrange,repeat([Lx_time_mean],length(qrange)),color="royalblue", linewidth=6.0,linestyle="--")

g = 2
Lx_time_mean = mean(Lx_times[g]).time*10^-6
plot(qrange,[x.time for x in mean.(rf_times[g,:])]*10^-6,color="green", linewidth=6.0,label="10-Regular")
plot(qrange,repeat([Lx_time_mean],length(qrange)),color="green", linewidth=6.0,linestyle="--")

g = 3
Lx_time_mean = mean(Lx_times[g]).time*10^-6
plot(qrange,[x.time for x in mean.(rf_times[g,:])]*10^-6,color="red", linewidth=6.0,label="Erdos-Renyi")
plot(qrange,repeat([Lx_time_mean],length(qrange)),color="red", linewidth=6.0,linestyle="--")
legend(loc="lower center",ncol = 3,fontsize=15, bbox_to_anchor=(0.5, -0.3))

ylabel("Runtime (ms)",fontsize=20)
xlabel("q",fontsize=20)
xticks(fontsize=15)
yticks(fontsize=15)
PyPlot.grid(true)

xscale("log")
## Runtimes on benchmark datasets

subplot(1,2,2)

g = 4
Lx_time_mean = mean(Lx_times[g]).time*10^-6
plot(qrange,[x.time for x in mean.(rf_times[g,:])]*10^-6,color="royalblue", linewidth=6.0,label="Citeseer")
plot(qrange,repeat([Lx_time_mean],length(qrange)),color="royalblue", linewidth=6.0,linestyle="--")

g = 5
Lx_time_mean = mean(Lx_times[g]).time*10^-6
plot(qrange,[x.time for x in mean.(rf_times[g,:])]*10^-6,color="green", linewidth=6.0,label="Pubmed")
plot(qrange,repeat([Lx_time_mean],length(qrange)),color="green", linewidth=6.0,linestyle="--")
xlabel("q",fontsize=20)
xticks(fontsize=15)
yticks(fontsize=15)

xscale("log")
outputfolder = string("output/runtime/")
legend(loc="lower center",ncol = 2,fontsize=15, bbox_to_anchor=(0.5, -0.3))
# fig.subplots_adjust(bottom=0.25,left=0.12) # or whatever
PyPlot.grid(true)
tight_layout()

savefig(string(outputfolder,"computationtimes.pdf"))
