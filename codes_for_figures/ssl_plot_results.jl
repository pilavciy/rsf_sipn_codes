using JLD2
using PyPlot
using LaTeXStrings
using MLJ
using ColorSchemes
data = "citeseer"
outputfolder = string("./output/",data,"/")
rcParams = PyPlot.PyDict(PyPlot.matplotlib."rcParams")
rcParams["legend.framealpha"] = 1.0
JLD2.@load string(outputfolder,"workspace.jld2") perf_exact perf_tilde perf_bar exaequo_t exaequo_b perf_exact_lp perf_tilde_lp randperformance constantperformance M NREP_TR NREP_LP MU mu_coll err_coll loocv_nrep sigma ntrials
### PERFORMANCE RESULTS ####
## Generalized SSL
rcParams = PyPlot.PyDict(PyPlot.matplotlib."rcParams")
rcParams["figure.figsize"] = [12,18]
fig= figure()
subplot(3,2,1)
title("Generalized SSL",size=25)
plot(M, mean(perf_exact[:,:], dims=2),label=latexstring("\$ \\hat{x}\$"), color="black", linewidth=6.0)
# nnr=1;plot(M, mean(perf_tilde[:,nnr,:], dims=2),linestyle="--",linewidth=6.0, color="cornflowerblue", label=latexstring("\$ \\tilde{x}\$ \$N_{TR,LP}\$=$(NREP_TR[nnr]), $(NREP_LP[nnr])"))
# nnr=3;plot(M, mean(perf_tilde[:,nnr,:], dims=2),linestyle="--",linewidth=6.0, color="royalblue", label=latexstring("\$ \\tilde{x}\$ \$N_{TR,LP}\$=$(NREP_TR[nnr]), $(NREP_LP[nnr])"))

nnr=1;plot(M, mean(perf_bar[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="lime", label=latexstring("\$ \\bar{x}\$ \$N_{TR,LP}\$=$(NREP_TR[nnr]), $(NREP_LP[nnr])"))
nnr=3;plot(M, mean(perf_bar[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="green", label=latexstring("\$ \\bar{x}\$ \$N_{TR,LP}\$=$(NREP_TR[nnr]), $(NREP_LP[nnr])"))
plot(M, repeat([randperformance],length(M),),linestyle="--",linewidth=3.0, color="red", label="Random Classifier")
plot(M, repeat([constantperformance],length(M),),linestyle="--",linewidth=3.0, color="salmon", label="Constant Classifier")
xlabel("m",fontsize=25)
ylabel("Citeseer\nAccuracy",fontsize=25)
xlim(minimum(M), maximum(M))
ylim(0,1)
xticks(fontsize= 20)
yticks(fontsize= 20)
PyPlot.grid(true)


## LP
subplot(3,2,2)
title("Label Propagation",size=25)
plot(M, mean(perf_exact_lp[:,:], dims=2), color="black", linewidth=6.0)
nnr=1;plot(M, mean(perf_tilde_lp[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="lime")
nnr=3;plot(M, mean(perf_tilde_lp[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="green")

plot(M, repeat([randperformance],length(M),),linestyle="--",linewidth=3.0, color="red")
plot(M, repeat([constantperformance],length(M),),linestyle="--",linewidth=3.0, color="salmon")

xlabel("m",fontsize=25)

xlim(minimum(M), maximum(M))
ylim(0,1)
xticks(fontsize= 20)
yticks(fontsize= 20)
PyPlot.grid(true)

## CORA
data = "cora"
outputfolder = string("./output/",data,"/")

JLD2.@load string(outputfolder,"workspace.jld2") perf_exact perf_tilde perf_bar exaequo_t exaequo_b perf_exact_lp perf_tilde_lp randperformance constantperformance M NREP_TR NREP_LP MU mu_coll err_coll loocv_nrep sigma ntrials
### PERFORMANCE RESULTS ####
## Generalized SSL
subplot(3,2,3)
# title("Generalized SSL",size=20)
plot(M, mean(perf_exact[:,:], dims=2), color="black", linewidth=6.0)
# nnr=1;plot(M, mean(perf_tilde[:,nnr,:], dims=2),linestyle="--",linewidth=6.0, color="cornflowerblue")
# nnr=3;plot(M, mean(perf_tilde[:,nnr,:], dims=2),linestyle="--",linewidth=6.0, color="royalblue")

nnr=1;plot(M, mean(perf_bar[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="lime")
nnr=3;plot(M, mean(perf_bar[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="green")
plot(M, repeat([randperformance],length(M),),linestyle="--",linewidth=3.0, color="red")
plot(M, repeat([constantperformance],length(M),),linestyle="--",linewidth=3.0, color="salmon")
xlabel("m",fontsize=25)
ylabel("Cora\nAccuracy",fontsize=25)
xlim(minimum(M), maximum(M))
ylim(0,1)
xticks(fontsize= 20)
yticks(fontsize= 20)
PyPlot.grid(true)


## LP
subplot(3,2,4)
# title("Label Propagation",size=20)
plot(M, mean(perf_exact_lp[:,:], dims=2), color="black", linewidth=6.0)
nnr=1;plot(M, mean(perf_tilde_lp[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="lime")
nnr=3;plot(M, mean(perf_tilde_lp[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="green")

plot(M, repeat([randperformance],length(M),),linestyle="--",linewidth=3.0, color="red")
plot(M, repeat([constantperformance],length(M),),linestyle="--",linewidth=3.0, color="salmon")

xlabel("m",fontsize=25)

xlim(minimum(M), maximum(M))
ylim(0,1)
xticks(fontsize= 20)
yticks(fontsize= 20)
PyPlot.grid(true)

## PUBMED
data = "pubmed"
outputfolder = string("./output/",data,"/")

JLD2.@load string(outputfolder,"workspace.jld2") perf_exact perf_tilde perf_bar exaequo_t exaequo_b perf_exact_lp perf_tilde_lp randperformance constantperformance M NREP_TR NREP_LP MU mu_coll err_coll loocv_nrep sigma ntrials
### PERFORMANCE RESULTS ####
## Generalized SSL
subplot(3,2,5)
# title("Generalized SSL",size=20)
plot(M, mean(perf_exact[:,:], dims=2), color="black", linewidth=6.0)
# nnr=1;plot(M, mean(perf_tilde[:,nnr,:], dims=2),linestyle="--",linewidth=6.0, color="cornflowerblue")
# nnr=3;plot(M, mean(perf_tilde[:,nnr,:], dims=2),linestyle="--",linewidth=6.0, color="royalblue")

nnr=1;plot(M, mean(perf_bar[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="lime")
nnr=3;plot(M, mean(perf_bar[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="green")
plot(M, repeat([randperformance],length(M),),linestyle="--",linewidth=3.0, color="red")
plot(M, repeat([constantperformance],length(M),),linestyle="--",linewidth=3.0, color="salmon")
xlabel("m",fontsize=25)
ylabel("Pubmed\nAccuracy",fontsize=25)
xlim(minimum(M), maximum(M))
ylim(0,1)
xticks(fontsize= 20)
yticks(fontsize= 20)
PyPlot.grid(true)


## LP
subplot(3,2,6)
# title("Label Propagation",size=20)
plot(M, mean(perf_exact_lp[:,:], dims=2), color="black", linewidth=6.0)
nnr=1;plot(M, mean(perf_tilde_lp[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="lime")
nnr=3;plot(M, mean(perf_tilde_lp[:,nnr,:], dims=2),linestyle="-",linewidth=6.0, color="green")

plot(M, repeat([randperformance],length(M),),linestyle="--",linewidth=3.0, color="red")
plot(M, repeat([constantperformance],length(M),),linestyle="--",linewidth=3.0, color="salmon")

xlabel("m",fontsize=25)

xlim(minimum(M), maximum(M))
ylim(0,1)
xticks(fontsize= 20)
yticks(fontsize= 20)
PyPlot.grid(true)

figlegend(loc="upper center",ncol = 3,fontsize=20, bbox_to_anchor=(0.55, 0.137))
fig.subplots_adjust(bottom=0.15,left=0.12) # or whatever
outputfolder = string("./output/")
tight_layout()
savefig(string(outputfolder,"accuracy_gSSLvsLP.pdf"))
