using RandomForests
using LightGraphs
using GraphIO
using StatsBase
using Clustering
using SparseArrays
using LinearAlgebra
using MLJ
using JLD2

include("utils.jl")
include("ssl_data_loaders.jl")
datasets = ["citeseer","cora"]

for data in datasets
    if(data == "cora")
        G , gr_truth = load_cora()
    elseif(data == "citeseer")
        G , gr_truth = load_citeseer()
    elseif(data == "pubmed")
        G , gr_truth = load_PubMed()
    end

    outputfolder = string("output/",data,"/")
    println("Output Folder: $outputfolder")
    try
        mkdir(outputfolder)
    catch
        println("$outputfolder already exists")
    end
    nvert = nv(G)
    nedge = ne(G)
    gr_truth = categorical(gr_truth)
    classnum = length(unique(gr_truth))
    encoded_labels = encode_labels(gr_truth,classnum)
    class_dist = sum(encoded_labels,dims=1)
    println("Dataset: $data")
    println("#nodes = $nvert, #edges = $nedge")
    println("Average degree: $(mean(degree(G)))")
    println("Class distribution: $class_dist")

    M = [20, 25, 30] # number of labeled nodes per block

    sigma = 0. # which Laplacian to use for smoothness
    MU = [0.01,0.1,1.0,10.0]
    NREP_TR = [50, 100, 500, 1000] # number of RSF replicates for estimators
    NREP_LP = [5, 10, 50, 100] # number of RSF replicates for estimators
    ntrials = 50
    loocv_nrep = 30

    perf_exact = zeros(length(M), ntrials)
    perf_tilde = zeros( length(M), length(NREP_TR), ntrials)
    perf_bar = zeros( length(M), length(NREP_TR), ntrials)
    exaequo_t = zeros( length(M), length(NREP_TR), ntrials)
    exaequo_b = zeros( length(M), length(NREP_TR), ntrials)

    ## Label Prop. PERFORMANCES

    perf_exact_lp = zeros(length(M), ntrials)
    perf_tilde_lp = zeros(length(M),length(NREP_LP), ntrials)

    mu_coll = zeros(length(M),ntrials,3)
    err_coll = zeros(length(M),ntrials,3,length(MU))
    randperformance =  1/classnum
    constantperformance =  maximum(sum(encoded_labels,dims=1)./sum(encoded_labels))
    for nn=1:ntrials
        for mm=1:length(M)
            m = M[mm]
            Y,labeled_nodes = sample_labels(encoded_labels,m)
            unlabeled_nodes = setdiff(1:nv(G),labeled_nodes)
            mu_hat,err_hat = ssl_LOOCV(G,Y,labeled_nodes,mu_range=MU,method="exact")
            mu_tilde,err_tilde = ssl_LOOCV(G,Y,labeled_nodes,mu_range=MU,nrep=loocv_nrep,method="xtilde")
            mu_bar,err_bar = ssl_LOOCV(G,Y,labeled_nodes,mu_range=MU,nrep=loocv_nrep,method="xbar")

            mu_coll[mm,nn,1] = mu_hat
            mu_coll[mm,nn,2] = mu_tilde
            mu_coll[mm,nn,3] = mu_bar

            err_coll[mm,nn,1,:] .= err_hat
            err_coll[mm,nn,2,:] .= err_tilde
            err_coll[mm,nn,3,:] .= err_bar

            ### TIKHONOV REGULARIZATION ###
            est_cl_exact, _ = ssl(G,Y,mu_hat,sigma,method="exact")
            perf_exact[ mm, nn] = accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_exact[unlabeled_nodes]))[1]
            for nnr=1:length(NREP_TR)
                nrep = NREP_TR[nnr]
                print("mu_hat=$mu_hat, mu_tilde=$mu_tilde, mu_bar=$mu_bar and m=$m and nn=$nn and nrep=$nrep\n")
                est_cl_tilde, exaequo_t[ mm, nnr, nn] = ssl(G,Y,mu_tilde,sigma,method="xtilde",nrep=nrep)
                perf_tilde[ mm, nnr, nn] = accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_tilde[unlabeled_nodes]))[1]
                est_cl_bar, _ = ssl(G,Y,mu_bar,sigma,method="xbar", nrep=nrep)
                perf_bar[ mm, nnr, nn] = accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_bar[unlabeled_nodes]))[1]
            end
            ### LABEL PROPAGATION ###
            rootset = labeled_nodes
            est_cl_exact_lp, _ = ssl_rootset(G,Y,0.0,sigma,method="exact",rootset=rootset)
            perf_exact_lp[ mm, nn] = accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_exact_lp[unlabeled_nodes]))[1]
            for nnr=1:length(NREP_LP)
                nrep = NREP_LP[nnr]
                print("m=$m and nn=$nn and nrep=$nrep\n")
                est_cl_tilde_lp,_ = (ssl_rootset(G,Y,0.0,sigma,method="xtilde",nrep=nrep,rootset=rootset))
                perf_tilde_lp[ mm, nnr, nn] = accuracy(gr_truth[unlabeled_nodes], categorical(est_cl_tilde_lp[unlabeled_nodes]))[1]
            end
        end
    end
    @save string(outputfolder,"workspace.jld2") perf_exact perf_tilde perf_bar exaequo_t exaequo_b perf_exact_lp perf_tilde_lp randperformance constantperformance M NREP_TR NREP_LP MU mu_coll err_coll loocv_nrep sigma ntrials
end
