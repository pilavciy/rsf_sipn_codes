using JLD2
using PyPlot
using LaTeXStrings
using MLJ
using ColorSchemes
using BenchmarkTools

outputfolder = string("./output/imagedenoising/")
JLD2.@load string(outputfolder,"workspace.jld2") MU σ nrep nrepsure scores_tilde scores_bar scores_hat mu_tilde mu_bar mu_hat noisy_psnr xhat_psnr xtilde_psnr xbar_psnr xtilde_1rep_psnr xbar_1rep_psnr

#### GAUSSIAN NOISE #####
fig = figure(figsize=[5,5])
# plot(MU, scores_tilde, color="blue",linewidth=6.0, label="SURE with \$ \\tilde{x}\$")
plot(MU, scores_bar, color="orange",linewidth=6.0, label="SURE with \$ \\bar{x}\$")
plot(MU, scores_hat, color="black",linewidth=6.0, label="SURE with \$ \\hat{x}\$")
xlabel("Parameter μ",fontsize=20)
ylabel("SURE Scores",fontsize=20)
xticks(fontsize=15)
yticks(fontsize=15)
PyPlot.grid(true)
tight_layout()
# fig.subplots_adjust(bottom=0.15) # or whatever
legend(fontsize=20)
# legend(loc="lower center",ncol = 3,fontsize=15, bbox_to_anchor=(0.5, 0.0))

savefig(string(outputfolder,"SURE_Scores.pdf"))

JLD2.@load string(outputfolder,"workspace_poisson.jld2") increment_hat increment_tilde increment_bar loss_hat loss_tilde loss_bar mu nrep maxiter tol noisy_psnr xhat_psnr xtilde_psnr xbar_psnr x y xhat xtilde xbar

#### POISSON NOISE #####
fig = figure(figsize=[5,5])
# plot((1:length(loss_tilde)), loss_tilde, color="blue",linewidth=6.0, label="Updates with \$ \\tilde{x}\$")
plot((1:length(loss_bar)), loss_bar, color="orange",linewidth=6.0, label="Updates with \$ \\bar{x}\$")
plot((1:length(loss_hat)), loss_hat, color="black",linewidth=6.0, label="Updates with \$ \\hat{x}\$")
xlabel("Iterations",fontsize=20)
ylabel("Loss Function",fontsize=20)
xticks(fontsize=15)
yticks(fontsize=15)
PyPlot.grid(true)
tight_layout()
xscale("log")
yscale("log")

legend(fontsize=20)

savefig(string(outputfolder,"loss_poisson.pdf"))
