using RandomForests

using ImageView, TestImages, Gtk.ShortNames, Images, ImageMagick, ImageQualityIndexes
using LightGraphs
using StatsBase
using Clustering
using Distributions
using BenchmarkTools
using SparseArrays
using LinearAlgebra
using JLD2
using PoissonRandom
include("utils.jl")

outputfolder = string("./codes/codes_for_figures/output/imagedenoising/")
println("Output Folder: $outputfolder")
try
    mkdir(outputfolder)
catch
    println("output folder already exists")
end
begin
    #### GAUSSIAN NOISE and TIKHONOV REGULARIZATION #####
    imname = "lake_gray"
    image = imresize(testimage(imname), 64, 64)
    image = Float64.(Gray.(image))
    nx = size(image,1)
    ny = size(image,2)
    rs = (v) -> reshape(v,nx,ny)
    G = LightGraphs.grid([nx,ny])
    image = image[:]

    σ = 0.2
    im_noisy = image .+ randn(length(image))*σ
    MU = collect(0.5:0.5:5.)
    nrep = 20
    nrepsure = 10
    mu_hat,scores_hat = SURE(G,im_noisy;σ=σ,mu_range=MU,nrep = nrepsure,method="exact")

    mu_tilde,scores_tilde = SURE(G,im_noisy;σ=σ,mu_range=MU,nrep = nrepsure,method="xtilde")
    mu_bar,scores_bar = SURE(G,im_noisy;σ=σ,mu_range=MU,nrep = nrepsure,method="xbar")

    xhat = smooth(G,mu_hat,im_noisy)
    xhat = rs(xhat)

    xtilde = smooth_rf(G,mu_tilde,im_noisy;variant=1,nrep=nrep).est
    xtilde = rs(xtilde)

    xbar = smooth_rf(G,mu_bar,im_noisy;variant=2,nrep=nrep).est
    xbar = rs(xbar)

    xtilde_1rep = smooth_rf(G,mu_tilde,im_noisy;variant=1,nrep=1).est
    xtilde_1rep = rs(xtilde_1rep)

    xbar_1rep = smooth_rf(G,mu_bar,im_noisy;variant=2,nrep=1).est
    xbar_1rep = rs(xbar_1rep)

    y = rs(im_noisy)
    noisy_psnr = (ImageQualityIndexes.assess_psnr(y, image))
    xhat_psnr = (ImageQualityIndexes.assess_psnr(xhat, image))
    xtilde_psnr = (ImageQualityIndexes.assess_psnr(xtilde, image))
    xbar_psnr = (ImageQualityIndexes.assess_psnr(xbar, image))

    xtilde_1rep_psnr = (ImageQualityIndexes.assess_psnr(xtilde_1rep, image))
    xbar_1rep_psnr = (ImageQualityIndexes.assess_psnr(xbar_1rep, image))

    display("y psnr: $noisy_psnr")
    display("xhat psnr: $xhat_psnr")
    display("xtilde psnr : $xtilde_psnr")
    display("xbar psnr : $xbar_psnr")
    display("xtilde_1rep psnr : $xtilde_1rep_psnr")
    display("xbar_1rep psnr : $xbar_1rep_psnr")

    y[y.<0] .= 0.; y[y.>1] .= 1.
    xhat[xhat.<0] .= 0.; xhat[xhat.>1] .= 1.
    xtilde[xtilde.<0] .= 0.; xtilde[xtilde.>1] .= 1.
    xbar[xbar.<0] .= 0.; xbar[xbar.>1] .= 1.
    xtilde_1rep[xtilde_1rep.<0] .= 0.; xtilde_1rep[xtilde_1rep.>1] .= 1.
    xbar_1rep[xbar_1rep.<0] .= 0.; xbar_1rep[xbar_1rep.>1] .= 1.

    save(string(outputfolder,"gaussian_orig.jpg"), rs(image))
    save(string(outputfolder,"gaussian_noisy.jpg"), (y))
    save(string(outputfolder,"gaussian_xhat_mu=$mu_hat.jpg"), (xhat))
    save(string(outputfolder,"gaussian_xtilde_mu=$(mu_tilde)_nrep=$nrep.jpg"), (xtilde))
    save(string(outputfolder,"gaussian_xbar_mu=$(mu_bar)_nrep=$nrep.jpg"), (xbar))
    save(string(outputfolder,"gaussian_xtilde_mu=$(mu_tilde)_nrep=1.jpg"), (xtilde_1rep))
    save(string(outputfolder,"gaussian_xbar_mu=$(mu_bar)_nrep=1.jpg"), (xbar_1rep))

    @save string(outputfolder,"workspace.jld2") MU σ nrep nrepsure scores_tilde scores_bar scores_hat mu_tilde mu_bar mu_hat noisy_psnr xhat_psnr xtilde_psnr xbar_psnr xtilde_1rep_psnr xbar_1rep_psnr
end
### POISSON NOISE and NEWTON'S METHOD #####
begin
    imname = "peppers"
    image = Gray.(imresize(testimage(imname),128,128))
    rescale = 25.0

    nx = size(image,1)
    ny = size(image,2)
    rs = (v) -> reshape(v,nx,ny)
    G = LightGraphs.grid([nx,ny])
    scaledimage = (rescale .* image)
    im_noisy = pois_rand.(scaledimage)
    im_noisy .= Int64.(im_noisy)

    x = zeros(size(image))
    xtilde = zeros(size(image))
    xbar = zeros(size(image))
    y = im_noisy
    y = y[:]

    z0 = (rand(nv(G)))
    mu = 0.1
    nrep = 20
    maxiter = 100
    tol = 10^(-10)
    xhat,increment_hat,loss_hat = newton(G,y,z0,mu;numofiter = maxiter,tol=tol, method="exact")
    xtilde,increment_tilde,loss_tilde = newton(G,y,z0,mu;numofiter = maxiter,tol=tol, method="xtilde",nrep=nrep)
    xbar,increment_bar,loss_bar = newton(G,y,z0,mu;numofiter = maxiter,tol=tol, method="xbar",nrep=nrep)

    y = rs(y/rescale)
    xhat = rs(xhat/rescale)
    xtilde = rs(xtilde/rescale)
    xbar = rs(xbar/rescale)

    noisy_psnr = (ImageQualityIndexes.assess_psnr(y, image))
    xhat_psnr = (ImageQualityIndexes.assess_psnr(xhat, image))
    xtilde_psnr = (ImageQualityIndexes.assess_psnr(xtilde, image))
    xbar_psnr = (ImageQualityIndexes.assess_psnr(xbar, image))

    display("y psnr: $noisy_psnr")
    display("xhat psnr: $xhat_psnr")
    display("xtilde psnr : $xtilde_psnr")
    display("xbar psnr : $xbar_psnr")

    @save string(outputfolder,"workspace_poisson.jld2") increment_hat increment_tilde increment_bar loss_hat loss_tilde loss_bar mu nrep maxiter tol noisy_psnr xhat_psnr xtilde_psnr xbar_psnr x y xhat xtilde xbar

    y[y.<0] .= 0.; y[y.>1] .= 1.
    xhat[xhat.<0] .= 0.; xhat[xhat.>1] .= 1.
    xtilde[xtilde.<0] .= 0.; xtilde[xtilde.>1] .= 1.
    xbar[xbar.<0] .= 0.; xbar[xbar.>1] .= 1.

    save(string(outputfolder,"poisson_orig.jpg"), image)
    save(string(outputfolder,"poisson_noisy.jpg"), y)
    save(string(outputfolder,"poisson_xhat_mu=$mu.jpg"), xhat)
    save(string(outputfolder,"poisson_xtilde_mu=$(mu)_nrep=$nrep.jpg"), xtilde)
    save(string(outputfolder,"poisson_xbar_mu=$(mu)_nrep=$nrep.jpg"), xbar)

end
